.. _RunSimulations:

Run Simulations
===============

To run a simulation, use the executable ``random_walk`` (see :ref:`BuildAndInstall` for instructions
 how to generate it and add it to your ``$PATH``). 
