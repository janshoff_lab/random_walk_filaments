.. _BuildAndInstall:

Build and Install
~~~~~~~~~~~~~~~~~

Build
=====

Random Walk Filaments uses meson as a build tool. Install it via

.. code:: shell

   $ pip install meson

You might have to add the ``--user`` option, and maybe use ``pip3`` instead of ``pip``, depending
on the configuration of your system.

Navigate to the project's top level directory, which should include the file ``meson.build``.
Execute the command

.. code:: shell

   $ meson builddir

This creates the directory ``builddir``. Navigate into that folder, and run

.. code:: shell

   $ meson test

This builds the Random Walk Filaments library and executables, and performs unit tests.
It generates two executable files,

* ``random_walk``, and
* ``random-walk-create-config``.

Usage of that files is described in :ref:`RunSimulations`.

Install
=======
