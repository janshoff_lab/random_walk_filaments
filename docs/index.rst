.. random_walk_filaments documentation master file, created by
   sphinx-quickstart on Fri Jun 21 14:10:00 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation of the Random Walk Filaments Simulation Framework
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

About
=====
   
Random Walk Filaments is a framework to run simulations of biopolymers, with a heavy
focus on properties of actin filaments for now.


Contents
========

.. toctree::
   :maxdepth: 2

   install
   run_simulations
   model
   parameters
   api/library_root
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
