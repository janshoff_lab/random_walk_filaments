#include <iostream>
#include <iomanip>
#include <ctime>
#include <random_walk_filaments.hpp>


using namespace random_walk_filaments;

int main(int argc, char** argv) {
  if (argc != 2) {
    std::cout << "Program requires one argument:" << std::endl;
    std::cout << "    path to output file" << std::endl;
    return 1;
  }

  nlohmann::json j;
  // set all default values:
  ConfigIO::add_version(j, VERSION);
  
  Vector3D box;
  box.x = 50.;
  box.y = 50.;
  box.z = 50.;
  ConfigIO::add_assembly_rate(j, 0.005);
  ConfigIO::add_box_size(j, box);
  ConfigIO::add_disassembly_rate(j, 0.002);
  ConfigIO::add_n_beads(j, 20000);
  ConfigIO::add_n_filaments(j, 5000);
  ConfigIO::add_n_steps(j, 20000);
  ConfigIO::add_k_stretch(j, 20.0);
  ConfigIO::add_k_bend(j, 22.0);
  ConfigIO::add_k_harmonic_repulsion(j, 40.0);
  ConfigIO::add_nucleation_rate(j, 0.01);
  ConfigIO::add_n_cells_for_distance_grid(j, {0, 0, 0});
  ConfigIO::add_time_step(j, 1.0e-3);
  ConfigIO::add_annealing(j, 100, 1.0, 0.1);
  
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream date;
  date << std::put_time(&tm, "%Y-%m-%d %H:%M");

  ConfigIO::add_date(j, date.str());

  std::string outfile = argv[1];
  std::ofstream config_out_file_stream(argv[1]);
  config_out_file_stream << std::setw(4) << j << std::endl;
  config_out_file_stream.close();
  std::cout << "wrote default config json to " << outfile << std::endl;
  return 0;
}
