#include <iostream>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <time.h>
#include <random_walk_filaments.hpp>


using namespace random_walk_filaments;

int main(int argc, char** argv) {
  if (argc < 4) {
    std::cout << "Program requires at least 3 arguments:" << std::endl;
    std::cout << "    1. path to output folder." << std::endl;
    std::cout << "    2. path to config file," << std::endl;
    std::cout << "    3. write interval," << std::endl;
    std::cout << "   (4. log level, default is info (debug, info, warning or error)" << std::endl;
    std::cout << "   (5. true, if you want to use random seed from config file)" << std::endl;
    std::cout << "   (6. path to file with bead data)" << std::endl;
    std::cout << "   (7. path to matching file with filaments)" << std::endl;    
    return 1;
  }
  std::string output_folder = argv[1];
  std::string path_to_config = argv[2];
  unsigned write_interval = atoi(argv[3]);  
  
  std::string log_level = "info";
  if (argc > 4) {
    log_level = argv[4];
  }
  std::string path_to_initial_coords;
  if (argc > 6) {
    path_to_initial_coords = argv[6];
  }
  std::string path_to_initial_filaments;
  if (argc > 7) {
    path_to_initial_filaments = argv[7];
  }
  std::string use_random_seed = "false";
  if (argc > 5) {
    use_random_seed = argv[5];
    if ((use_random_seed != "true") & (use_random_seed != "false")) {
      throw std::invalid_argument(R"(use seed flag (6th parameter) has to be "true" or "false")");
    }
  } 
  
  // set up logger
  Logger logger;
  logger.set_log_stream(&std::cout);
  // std::ofstream *log_stream;
  // log_stream = new std::ofstream("log.txt");
  // logger.set_log_stream( log_stream );
  if (log_level == "info") logger.set_log_up_to(Logger::LOG_LEVEL_INFO);
  else if (log_level == "debug") logger.set_log_up_to(Logger::LOG_LEVEL_DEBUG);
  else if (log_level == "warning") logger.set_log_up_to(Logger::LOG_LEVEL_WARNING);
  else if (log_level == "error") logger.set_log_up_to(Logger::LOG_LEVEL_ERROR);
  else {
    throw std::invalid_argument("log level argument (5th parameter) has to be one of these: debug, info, warning or error");
  }
  logger.log(Logger::LOG_LEVEL_INFO, "Logger was set up.");
  std::ostringstream log_string_stream;


  // read json from config file
  std::ifstream config_file_stream(path_to_config);
  nlohmann::json j;
  config_file_stream >> j;
  config_file_stream.close();

  // check version of config
  std::string config_version = ConfigIO::get_version(j);
  if (VERSION != config_version) {
    log_string_stream << "config version is '" << config_version << "', but your version of "
		      << argv[0] << " is '" << VERSION << "'!";
    logger.log(Logger::LOG_LEVEL_WARNING, log_string_stream);
    // @todo interaction how to proceed.
  }
  
  // parse json to variables
  double assembly_rate = ConfigIO::get_assembly_rate(j);
  Vector3D box_size = ConfigIO::get_box_size(j);
  double disassembly_rate = ConfigIO::get_disassembly_rate(j);
  unsigned n_beads = ConfigIO::get_n_beads(j);
  unsigned n_filaments = ConfigIO::get_n_filaments(j);
  unsigned n_steps = ConfigIO::get_n_steps(j);
  double k_stretch = ConfigIO::get_k_stretch(j);
  double k_bend = ConfigIO::get_k_bend(j);
  double nucleation_rate = ConfigIO::get_nucleation_rate(j);
  cell_index_t n_cells = ConfigIO::get_n_cells_for_distance_grid(j);
  double time_step = ConfigIO::get_time_step(j);
  double k_harmonic_repulsion = ConfigIO::get_k_harmonic_repulsion(j);
  
 
  std::cout << "system box size = ("
	    << box_size.x << ", "
	    << box_size.y << ", "
	    << box_size.z << ")." << std::endl;
  
  // get new random seed  
  unsigned random_seed = time(NULL);
  if (use_random_seed == "true") {
    random_seed = ConfigIO::get_random_seed(j);    
    logger.log(Logger::LOG_LEVEL_WARNING, "using random seed from config file.");
  }
  log_string_stream << "random seed = " << random_seed;
  logger.log(Logger::LOG_LEVEL_INFO, log_string_stream);

  // set up RandomWrapper
  RandomWrapper wrapper;
  wrapper.set_seed(random_seed);
  wrapper.initialize();
  
  // set system up
  System system(n_beads, n_filaments);
  system.set_logger(&logger);  
  system.set_random_wrapper(&wrapper);
  // set up distance grid
  bool use_grid = true;
  for (unsigned nc: n_cells) {
    if (nc != 0) continue;
    use_grid = false;
    break;    
  } 
  
  system.set_k_bend(k_bend*time_step);
  system.set_k_stretch(k_stretch*time_step);
  system.set_noise_coeff(sqrt(2*time_step));
  system.set_harmonic_repulsion_coefficient(k_harmonic_repulsion * time_step);

  system.set_box_size(box_size);

  system.set_nucleation_rate(nucleation_rate);
  system.set_assembly_rate(assembly_rate);
  system.set_disassembly_rate(disassembly_rate);  


  

  
  // prepare to measure time passed
  struct timespec start, finish;
  double elapsed;
  // create beads (HAVE TO DO THIS BEFORE SETTING FILE WRITER!)
  
  if (path_to_initial_coords.empty() || path_to_initial_filaments.empty()) {
    std::cout << "will start without beads! will lead to undefined behaviour if no nucleation rate was set!" << std::endl;
  } else {
    std::cout << "will read bead positions from initial file" << std::endl;
    clock_gettime(CLOCK_MONOTONIC, &start);
    system.initialize_beads_via_files(path_to_initial_coords, path_to_initial_filaments);
    clock_gettime(CLOCK_MONOTONIC, &finish);    
  }    

  // unsigned actual_number_of_beads = system.get_number_of_beads();  
  // set up file-writer
  TextStackWriter file_writer(output_folder, write_interval, n_steps);
  system.set_bead_file_writer(&file_writer);

  elapsed = (finish.tv_sec - start.tv_sec);
  elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
  std::cout << "initialized beads in " << elapsed << " seconds" << std::endl;
  
  // compose new config json and write to output folder
  ConfigIO::add_random_seed(j, random_seed);
  // ConfigIO::add_n_beads(j, actual_number_of_beads);
  ConfigIO::add_version(j, VERSION);
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream date;
  date << std::put_time(&tm, "%Y-%m-%d %H:%M");
  ConfigIO::add_date(j, date.str());
  std::ofstream config_out_file_stream(output_folder + "/config.json");
  config_out_file_stream << std::setw(4) << j << std::endl;
  config_out_file_stream.close();

  unsigned annealing_n_steps;
  double annealing_displacement_cap, annealing_relative_dt;
  ConfigIO::get_annealing(j, annealing_n_steps,
                          annealing_relative_dt, annealing_displacement_cap);

  // run the simulation
  std::cout << "will now run simulation for " << n_steps << " time steps"
	    << std::endl << std::endl;
  clock_gettime(CLOCK_MONOTONIC, &start);
  if (use_grid) {
    
    std::cout << "Will use grid ("
	      << n_cells[0] << ", "
	      << n_cells[1] << ", "
	      << n_cells[2] << ") for collision handling."
	      << std::endl << std::endl;
    DistanceGrid dg(n_cells, box_size);
    dg.set_random_wrapper(&wrapper);
    dg.update_cell_map((std::vector<Bead>) {} , 0);
    system.set_distance_grid(&dg);

    if (annealing_n_steps != 0) {
      std::cout << "performing annealing for " << annealing_n_steps
                << " steps ...";
      system.perform_annealing(annealing_n_steps, annealing_relative_dt,
                               annealing_displacement_cap, use_grid);
      std::cout << " done" << std::endl;
    }

    for (unsigned i = 0; i < n_steps; i++) {
      if (i % write_interval == 0) {
	std::cout << "\rstep " << i << "/" << n_steps << std::flush;
      }
      //std::cout << "step " << i << "/" << n_steps << std::endl;
      //dg.print_state();
      dg.update_cell_map((std::vector<Bead>) {}, 0);
      system.move_one_step_with_distance_grid();
      system.handle_polymerization_events(i);
    }
    system.write_final_states(output_folder);
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    std::cout << std::endl
	      << "done. elapsed time: " << elapsed << " seconds" << std::endl;
    return 0;
  } else {

    if (annealing_n_steps != 0) {
      std::cout << "performing annealing for " << annealing_n_steps
                << " steps ...";
      system.perform_annealing(annealing_n_steps, annealing_relative_dt,
                               annealing_displacement_cap, use_grid);
      std::cout << " done" << std::endl;
    }

    for (unsigned i = 0; i < n_steps; i++) {
      if (i % write_interval == 0) {
	std::cout << "\rstep " << i << "/" << n_steps << std::flush;
      }
      system.move_one_step();
      system.handle_polymerization_events(i);
    }
    system.write_final_states(output_folder);
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    std::cout << std::endl
	      << "done. elapsed time: " << elapsed << " seconds" << std::endl;
    return 0;
  }
}
