#ifndef RANDOM_WALK_FILAMENTS_HPP
#define RANDOM_WALK_FILAMENTS_HPP

#include <vector>
#include <array>
#include <map>
#include <fstream>
#include <iostream>
#include <random>
#include <limits>
#include <json.hpp>
#include <cstdint>
#include <climits>
#include <cmath>

namespace random_walk_filaments {
  
  static const std::string VERSION = "v0.7.1";
  
  const double pi = 3.1415926535897932384626433;
  
  /** Enum of possible bead states.
   *  Beads can be in different states to represent
   *  segments of a filament e.g. with a cross-linker
   *  or myosin motor bound to it.
   */
  enum class BeadState {
    Free,
    FilamentInterior,
    FilamentBegin,
    FilamentEnd,
    Crosslink,
    Motor
  };

  /** Vector3D represents a single coordinate in a 3-dimensional space.
   * All kinds of vector calculus are / will be implemented in this class.
   */
  class Vector3D {
    
  public: // actual coordinates
    double x = 0.0;
    double y = 0.0;
    double z = 0.0;

  public: // altering methods

    void add(const Vector3D& v);
    void remove(const Vector3D& v);
    
  public: // unaltering methods
    
    /** Length of vector.
     * @return Length of vector.
     */
    double norm() const;

    /** Yields squared length of vector.
     * Like norm, but not taking the square root
     * in the last step. Can be more efficient,
     * depending on what you want to do,
     * since it saves a few computations.
     * @return Squared length of vector.
     */
    double norm_squared() const;
    
    /** Scalar product of a scalar with this vector.
     * Will return the result as a new vector, old vector remains unchanged.
     * @param c Scalar to multiply with.
     * @return Resulting vector.
     */
    Vector3D scalar_product(double c) const;
    
    /** Computes distance to another point.
     * @param v Vector pointing to other 3-dimensional coordinate
     * @return Distance to given point
     */
    double distance(const Vector3D &v) const;

    /** Computes squared distance to another point.
     * Does not take square root of the scalar product of the
     * relative vector, can save you some computations.
     * @param v Vector pointing to other 3-dimensional coordinate
     * @return Squared distance to given point.
     */
    double distance_squared(const Vector3D &v) const;

    bool isnan() const;

    double dot(const Vector3D &v) const;

  public: // static methods
    /** Sums two vectors.
     * Static method that sums two vectors. Input vectors remain unchanged,
     * the result will be returned in a new vector.
     * @param v1 Summand 1
     * @param v2 Summand 2
     * @return Resulting vector.
     */
    static Vector3D sum(const Vector3D &v1, const Vector3D &v2);
    /** Subtracts vector from another vector.
     * Computes difference between two vectors. Input vectors remain unchanged,
     * the result will be returned in a new vector.
     * @param v1 Minuend
     * @param v2 Subtrahend
     * @return Resulting vector.
     */
    static Vector3D subtract(const Vector3D &v1, const Vector3D &v2);
    /** To compute the shortest distance from a point to a path,
     * figure out where on the path the point of shortest distance is.
     * @param d Path that the moving bead wants to move along.
     * @param p Relative vector from current position of moving bead to a static bead.
     */
    static double factor_shortest_distance_to_path(const Vector3D &d,
						   const Vector3D &p);
    /** Create a vector of nans.
     */
    static Vector3D nan();

    /** Determines vector from v1 to v2 with periodic boundaries projection.
     *  The minimum image algorithm assures that the determined vector goes
     *  from that projection of v1 to v2 such that distance is minimized.
     *  @param v1 Vector to start point
     *  @param v2 Vector to end point
     *  @param box Specifies boundaries (0, box.x), (0, box.y), (0, box.z).
     *  @return Minimum image vector from v1 to v2
     */
    static Vector3D minimum_image_vector(const Vector3D &v1, const Vector3D &v2,
					 const Vector3D &box);
    /** Length of minimum image vector from v1 to v2.
     *  Calls System::minimum_image_vector and returns length of that vector.
     *  @param v1 Vector to start point
     *  @param v2 Vector to end point
     *  @param box Specifies boundaries (0, box.x), (0, box.y), (0, box.z).
     *  @return Length of minimum image vector from v1 to v2
     */
    static double minimum_image_distance_squared(const Vector3D &v1, const Vector3D &v2,
						 const Vector3D &box);
    
  public: // operators
    /** Not Implemented yet.
     */
    inline Vector3D operator+(const Vector3D &other) const;
  };

  typedef int index_link_t;

  /** A bead is defined via its position,
   * its state and its links to other beads.
   */
  struct Bead {
    Vector3D position;
    BeadState state;
    index_link_t previous = -1;
    index_link_t next = -1;
    index_link_t cross_filament = -1;
    unsigned id = UINT_MAX;
    static unsigned count_total;
    void copy_properties(const Bead &b) {
      position = b.position;
      state = b.state;
      previous = b.previous;
      next = b.next;
      cross_filament = b.cross_filament;
      id = b.id;
    }
  };

  /** A filament is defined by two links to beads,
   * one pointing to the beginning of the filament,
   * one pointing to the end.
   * There has to be a path following the links of the beads
   * going from first bead to the last.
   */
  struct Filament {
    const static unsigned ID_INVALID = UINT_MAX;
    index_link_t first;
    index_link_t last;
    unsigned id = ID_INVALID;
    unsigned n_beads_total = 0;
    static unsigned count_total;
    void copy_properties(const Filament &f) {
      first = f.first;
      last = f.last;
      id = f.id;
      n_beads_total = f.n_beads_total;
    }
  };

  
  class BeadFileWriter {
  public:
    /** Push current state of the simulation to the FileWriter, which
     * then handles writing of this state internally.
     * @param beads Vector containing all beads of the system.
     * @param filaments Vector containing all filaments of the system.
     * @param step Current step of the simulation.
     * @param n_valid_beads Number of beads that are valid in the vector of beads 
     *   (all beads with larger indices will be ignored upon writing).
     */
    virtual void push_bead_states(const std::vector<Bead> &beads,
				  const std::vector<Filament> &filaments,
				  unsigned step,
				  unsigned n_valid_beads) = 0;
    virtual ~BeadFileWriter() {};
  protected:
    virtual void create_output_format_description() = 0;
    const std::string FORMAT_DESCRIPTION_FILE = "output_format_description.txt";
  };

  class TextStackWriter: public BeadFileWriter {
  public: // interface
    void push_bead_states(const std::vector<Bead> &beads,
			  const std::vector<Filament> &filaments,
			  unsigned step,
			  unsigned n_valid_beads);    
  public: // constructor and destructor;
    TextStackWriter(std::string output_dir_, unsigned write_interval_,
		    unsigned n_time_steps_);
    ~TextStackWriter();

  private: // methods
    void create_output_format_description();
    bool do_write(unsigned t);
    void write_beads(const std::vector<Bead> &beads,
		     unsigned n_valid_beads, unsigned time_step);
    void write_filaments(const std::vector<Filament> &filaments,
			 unsigned time_step);

  private: // members
    unsigned write_interval;
    unsigned n_time_steps;
    
    std::ofstream* ofs_beads = NULL;
    std::ofstream* ofs_filaments = NULL;
    const std::string output_dir;

    const std::string FILENAME_BEADS = "beads.txt";
    const std::string FILENAME_FILAMENTS = "filaments.txt";
  };
  
  /** Handles writing of bead coordinates and bead-bead links to text files.      
   */
  class TextBeadFileWriter: public BeadFileWriter {
  public: // interface
    void push_bead_states(const std::vector<Bead> &beads,
			  const std::vector<Filament> &filaments,
			  unsigned step,
			  unsigned n_valid_beads);
    void close_files();    

  public: // constructor and Destructor
    TextBeadFileWriter(std::string output_dir, unsigned max_file_entries);
    ~TextBeadFileWriter();
  private:
    unsigned current_index = 0;
    void open_next_files();
    void create_output_format_description();
    
    std::ofstream* output_file_index_links = NULL;
    std::ofstream* output_file_coords = NULL;

    const unsigned max_file_entries;
    unsigned current_file_entries = 0;

    const std::string output_dir;

    const std::string FILE_SUFFIX = ".txt";
    const std::string FILE_PREFIX_COORDS = "bead_coords_";
    const std::string FILE_PREFIX_INDEX_LINKS = "bead_links_";
  };
  
  /** Interface to more comfortably draw numbers 
   *  from a random number generator.
   */
  class RandomWrapper {
  public:
    void initialize();
    unsigned draw_uint();
    double draw_double_in_range(double low, double high);
    void fill_vector_normal(std::vector<Vector3D> &v, unsigned n,
			    double std);
    double draw_normal(double mean, double std);
    Vector3D draw_random_unit_vector();
    std::vector<unsigned> generate_shuffled_index_vector(unsigned size);

  public: // setter
    void set_seed(unsigned seed);

  public: // getter
    unsigned get_seed() const;
    
  private:
    std::mt19937 random_engine;
    unsigned seed_value;
    std::uniform_int_distribution<unsigned> uint_dist;
    std::normal_distribution<double> normal_dist = std::normal_distribution<double>(0.0, 1.0);
    const unsigned RW_UINT_MAX = std::numeric_limits<unsigned>::max();
  };

  /** Handles logging, i.e. piping messages to a desired output stream.
   * Currently not in use anywhere ...
   */
  class Logger {
  public: // log levels
    static const int LOG_LEVEL_DEBUG = 0;
    static const int LOG_LEVEL_INFO = 1;
    static const int LOG_LEVEL_WARNING = 2;
    static const int LOG_LEVEL_ERROR = 3;

  public: // interface
    void log(int level, std::ostringstream &message_stream);
    void log(int level, std::string message);
    void set_log_prefix(std::string prefix);
    void revert_to_previous_prefix();

  public: // set up methods (call directly after initialization)
    void set_log_up_to(int level);
    void set_log_stream(std::ostream* stream);

  private: // members
    int log_up_to = 2;
    std::vector<std::string> prefixes;
    std::ostream* log_stream = &std::cout;
    
  };    

  /** NOT FOR DIRECT USE! ONLY FOR TESTING! INHERITED BY class System!
   */
  class FilamentHandler {
  public: // interface
    /** Allocate memory for bead vector and valid_beads vector.
     */
    void reserve_space_for_beads_and_filaments(uint32_t space_beads,
					       uint16_t space_filaments);
    
    void add_bead_to_filament(unsigned id, BeadState which_end=BeadState::FilamentEnd);
    void remove_bead_from_filament(unsigned id, BeadState which_end=BeadState::FilamentBegin);
    unsigned create_new_filament();
    bool check_for_collision(const Vector3D &v);

    void fix_links_to_copied_bead(unsigned new_idx, unsigned old_idx);
    
    Vector3D get_new_bead_location(Vector3D location_prev, Vector3D location_prev_prev);

    /** Determines vector from v1 to v2 with periodic boundaries projection.
     *  The minimum image algorithm assures that the determined vector goes
     *  from that projection of v1 to v2 such that distance is minimized.
     *  @param v1 Vector to start point
     *  @param v2 Vector to end point
     *  @return Minimum image vector from v1 to v2
     */
    Vector3D minimum_image_vector(const Vector3D &v1, const Vector3D &v2);
    /** Length of minimum image vector from v1 to v2.
     *  Calls System::minimum_image_vector and returns length of that vector.
     *  @param v1 Vector to start point
     *  @param v2 Vector to end point
     *  @return Length of minimum image vector from v1 to v2
     */
    double minimum_image_distance_squared(const Vector3D &v1, const Vector3D &v2);

  public: // members
    double new_segment_length_std = 0.1;
    
    Vector3D box_size;
    Logger* logger = nullptr;

    RandomWrapper* random_wrapper;

    std::vector<Bead> beads;
    std::vector<Filament> filaments;

    uint32_t n_valid_beads = 0;    
  };


  typedef std::array<unsigned, 3>  cell_index_t;
  typedef std::map<cell_index_t, std::vector<unsigned>> cell_map_t;

  class DistanceGrid {
  public: // constructor
    
    DistanceGrid(const cell_index_t n_cells_,
		 const Vector3D box_) : n_cells(n_cells_), box(box_){
      cell_length.x = box.x/n_cells[0];
      cell_length.y = box.y/n_cells[1];
      cell_length.z = box.z/n_cells[2];
      create_full_cell_indices_vector();
      create_empty_cell_map();      
    }

  public: // methods
    void clear_cell_map();
    const std::vector<unsigned>& get_bead_indices(const cell_index_t& cidx);
    std::vector<cell_index_t> get_forward_neighbors(const cell_index_t& cidx);
    cell_index_t coords_to_cidx(Vector3D coords);
    const std::vector<cell_index_t>& get_cell_indices();
    void set_random_wrapper(RandomWrapper* rw);
    void update_cell_map(const std::vector<Bead> &beads, unsigned n_valid_beads);

  private: // methods
    void create_empty_cell_map();
    unsigned get_valid_cell_index(int idx, unsigned max_idx);
    void create_full_cell_indices_vector();
    void add_bead(unsigned bidx, Vector3D coords);
    
    
  private: // attributes
    const cell_index_t n_cells;
    const Vector3D box;
    Vector3D cell_length;
    cell_map_t cell_map;
    std::vector<cell_index_t> cell_indices;
    RandomWrapper* random_wrapper;
  };


  class CollisionHandler {

  public:
    std::vector<Bead> *beads;
    Vector3D *box;
    Logger *logger;
    
  public:
    /** Checks if any bead is in the path defined by position_of_current_bead and its
     *  attempted displacement.
     *  @param index_of_current_bead Index of current bead in System's vector of beads.
     *  @param position_of_current_bead Spatial position of current bead.
     *  @param displacement Vector defining attempted movement of current bead.
     */
    std::multimap<double, unsigned> check_for_beads_in_path(unsigned index_of_current_bead,
							    const Vector3D &position_of_current_bead,
							    const Vector3D &displacement,
							    const std::vector<unsigned> &bidces);

    /** Computes a fraction of the path, at which a (moving) bead collides with another (fixed) bead.
     *  @param length_of_path Full length of the path, that the moving bead wants to move along.
     *  @param distance_to_path Distance of the center of the fixed bead to the path of the moving bead.
     *  @param factor_shortest_distance Fraction of the full path length, at which the distance of the 
     *     fixed bead to the path of the moving bead would be shortest.
     *  @return Fraction of the path at which the beads collide.
     */
    static double compute_factor_on_path_for_bead_diameter_distance(double length_of_path,
								    double distance_to_path,
								    double factor_shortest_distance);

    
  };
  
  /** Joins all the other classes to contain all the parts necessary
   *  to conduct random walk simulations.
   *  Actual simulations need to be run via repeatadly calling the 
   *  System::move_one_step method.
   */
  class System: protected FilamentHandler {
  
  public: // constructor
    System(uint32_t reserve_n_beads,
	   uint16_t reserve_n_filaments) {
      reserve_space_for_beads_and_filaments(reserve_n_beads, reserve_n_filaments);
      forces.reserve(reserve_n_beads);
      position_changes.reserve(reserve_n_beads);
      thermal_motion.reserve(reserve_n_beads);
      for (uint32_t i=0; i<reserve_n_beads; i++){
	Vector3D v; v.x=0.; v.y=0.; v.z=0.;
	forces.push_back(v);
	position_changes.push_back(v);
	thermal_motion.push_back(v);
      }
      
    }
    
  public: // interface
    void handle_polymerization_events(unsigned t_current);
    /** Main method to 'run' the simulation, should be called in a loop, one call 
     * of this function performs one simulation step (each bead is moved once).
     */
    void move_one_step();
    /** Same as move_one_step, but relying on distance grid to have less than
     *  N*(N-1) collision checks.
     *  Care! Only starting to be faster for around N ~ 5000.
     */
    void move_one_step_with_distance_grid();

    void compute_filament_forces();

    void handle_pair_interactions(const cell_index_t& cidx);

    void update_positions();

    void add_harmonic_repulsion(unsigned idx1, unsigned idx2);

    void perform_annealing(unsigned n_steps, double relative_dt, double displacement_cap,
                           bool use_grid);

    void initialize_beads_via_files(std::string path_to_initial_coords,
				    std::string path_to_initial_filaments);
    static Bead bead_line_to_bead_object(std::istringstream &line_stream);
    static Filament filament_line_to_filament_object(std::istringstream &line_stream);

    void write_final_states(std::string output_dir);

    Vector3D compute_forward_stretch(const Vector3D &v_n);
    Vector3D compute_forward_bend(const Vector3D &v_p,
				  const Vector3D &v_n,
				  const Vector3D &v_nn);


  private: // methods

    void link_collision_handler();
    
    void handle_assembly_events(unsigned t_current);
    void handle_disassembly_events(unsigned t_current);
    void handle_nucleation_events(unsigned t_current);
    void insert_next_assembly_event(unsigned t_current, unsigned filament_id);
    void insert_next_disassembly_event(unsigned t_current, unsigned filament_id);
    void update_positions_with_cap(double displacement_cap);
        
  private: // members

    std::vector<Vector3D> forces;
    std::vector<Vector3D> position_changes;
    std::vector<Vector3D> thermal_motion;

    CollisionHandler collision_handler;
    
    double noise_coeff;
    double k_stretch;
    double k_bend;
    double k_harmonic_repulsion;

    unsigned current_step = 0;

    BeadFileWriter* file_writer;

    std::multimap<unsigned, unsigned> assembly_events;
    std::multimap<unsigned, unsigned> disassembly_events;
    unsigned nucleation_event = 0;

    double nucleation_rate;
    double assembly_rate;
    double disassembly_rate;

    DistanceGrid* distance_grid;
        
  public: // setter
    void set_box_size(Vector3D box_size);
    void set_k_stretch(double k_s);
    void set_k_bend(double k_b);
    void set_assembly_rate(double r);
    void set_disassembly_rate(double r);
    void set_nucleation_rate(double r);
    void set_noise_coeff(double c);
    void set_harmonic_repulsion_coefficient(double k);

    void set_bead_file_writer(BeadFileWriter* writer);
    void set_random_wrapper(RandomWrapper* wrapper);
    void set_logger(Logger* logger_);

    void set_distance_grid(DistanceGrid* dg);

  public: // getter
    Vector3D get_box_size();
    unsigned get_number_of_beads();
    double   get_k_stretch();
    double   get_k_bend();
  };

  /** Provides static methods to store parameters in a json object
   *  or read parameters from a json object.
   */
  class ConfigIO {
  public: // add parameters to json
    static void add_assembly_rate(nlohmann::json &j, double rate);
    static void add_box_size(nlohmann::json &j, Vector3D box);
    static void add_date(nlohmann::json &j, std::string date_string);
    static void add_disassembly_rate(nlohmann::json &j, double rate);
    static void add_k_stretch(nlohmann::json &j, double k_stretch);
    static void add_k_bend(nlohmann::json &j, double k_bend);
    static void add_k_harmonic_repulsion(nlohmann::json &j, double k);
    static void add_n_beads(nlohmann::json &j, unsigned n_beads);
    static void add_n_filaments(nlohmann::json &j, unsigned n_filaments);
    static void add_n_steps(nlohmann::json &j, unsigned n_steps);
    static void add_nucleation_rate(nlohmann::json &j, double rate);
    static void add_n_cells_for_distance_grid(nlohmann::json &j, cell_index_t n_cells);
    static void add_annealing(nlohmann::json &j, unsigned n_steps, double relative_dt,
                              double displacement_cap);
    static void add_random_seed(nlohmann::json &j, int seed);
    static void add_time_step(nlohmann::json &j, double dt);
    static void add_version(nlohmann::json &j, std::string v);

  public: // read params from json
    static double get_assembly_rate(const nlohmann::json &j);
    static Vector3D get_box_size(const nlohmann::json &j);
    static void get_annealing(const nlohmann::json &j, unsigned& n_steps,
                              double& relative_dt, double& displacement_cap);
    static double get_disassembly_rate(const nlohmann::json &j);
    static double get_nucleation_rate(const nlohmann::json &j);
    static unsigned get_n_beads(const nlohmann::json &j);
    static cell_index_t get_n_cells_for_distance_grid(const nlohmann::json &j);
    static unsigned get_n_filaments(const nlohmann::json &j);
    static unsigned get_n_steps(const nlohmann::json &j);
    static double get_k_stretch(const nlohmann::json &j);
    static double get_k_bend(const nlohmann::json &j);
    static double get_k_harmonic_repulsion(const nlohmann::json &j);
    static int get_random_seed(const nlohmann::json &j);
    static double get_time_step(const nlohmann::json &j);
    static std::string get_version(const nlohmann::json &j);
  };  
}

#endif /* RANDOM_WALK_FILAMENTS_HPP */
