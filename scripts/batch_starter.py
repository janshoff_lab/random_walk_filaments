from typing import List, Union, Tuple
import numpy as np
import os
import json
from subprocess import run
from multiprocessing.dummy import Pool
from create_filament import create_filament, write_beads_file, write_filaments_file

ParamSet = List[Union[int, float]]
ParamList = List[ParamSet]

param_types = {'n_beads': int}

class BatchStarter:

    def __init__(self):
        self.config_template = None
        self.write_interval = None
        self.labels = None
        self.beads_file = None
        self.filaments_file = None
    
    @staticmethod
    def create_param_list(k_bend: List[float]=None,                          
                          k_stretch: List[float]=None,
                          time_step: List[float]=None,
                          n_beads: List[int]=None,
                          multiplier: int=1) -> Tuple[ParamList, List[str]]:

        labels = []
        params = []
        for p, lbl in zip([k_bend, k_stretch,
                           time_step, n_beads],
                          ['k_bend', 'k_stretch',
                           'time_step', 'n_beads']):
            if p is None:
                continue
            params.append(np.array(p))
            labels.append(lbl)

        n_param_sets = multiplier
        for p in params:
            n_param_sets = n_param_sets * len(p)
        param_sets = np.full((n_param_sets, len(params)), np.nan)

        p_set = np.array([p[0] for p in params])
        param_sets[:multiplier] = p_set        
        n_filled_sets = multiplier
        for j, p in enumerate(params):
            filled_sets = param_sets[:n_filled_sets]
            if filled_sets.ndim == 1:
                filled_sets = filled_sets[np.newaxis, :]
            for k, pp in enumerate(p):
                param_sets_k = filled_sets.copy()
                param_sets_k[:, j] = pp        
                param_sets[n_filled_sets*k: n_filled_sets*(k+1)] = param_sets_k
            n_filled_sets = n_filled_sets * len(p)
        return [list([i] + list(p)) for i, p in enumerate(param_sets)], labels

    def _write_tmp_json_and_initial_values(self, param_set: ParamSet) -> Tuple[str, str, str]:
        i = int(param_set[0])
        with open(self.config_template, 'rt') as f:
            config_json = json.load(f)

        for j in range(1, len(param_set)):
            p = param_set[j]
            lbl = self.labels[j-1]
            if lbl in param_types:
                config_json[lbl] = param_types[lbl](p)
            else:
                config_json[lbl] = p

        x = config_json['box_size']['x']
        y = config_json['box_size']['y']
        z = config_json['box_size']['z']

        box = np.array([x, y, z])
        n_beads = config_json['n_beads']
        k_stretch = config_json['k_stretch']
        k_bend = config_json['k_bend']
        time_step = config_json['time_step']

        if self.beads_file is None and self.filaments_file is None:
            coords, links = create_filament(n_beads, box, 1.0, 1.0,
                                            k_stretch, k_bend, time_step)

            fname_beads = '.tmp_beads_{:04}.txt'.format(i)
            fname_filaments = '.tmp_filaments_{:04}.txt'.format(i)

            write_beads_file(fname_beads, n_beads, coords, links)
            write_filaments_file(fname_filaments, n_beads)
            
        else:
            fname_beads = self.beads_file
            fname_filaments = self.filaments_file
        fname_config = '.tmp_config_{:04}.json'.format(i)

        json.dump(config_json, open(fname_config, 'wt'))

        return fname_config, fname_beads, fname_filaments

    def _start_sim(self, param_set: ParamSet):
        out_dir = 'out{:04}'.format(int(param_set[0]))
        try:
            os.mkdir(out_dir)
            print("created dir '{}'".format(out_dir))
        except FileExistsError:
            print("dir already exists: '{}'".format(out_dir))

        tmp_files = self._write_tmp_json_and_initial_values(param_set)            

        process_args = ["random_walk",
                        out_dir,
                        tmp_files[0],
                        str(self.write_interval),                        
                        self.log_level,
                        "false",
                        tmp_files[1],
                        tmp_files[2]]
        if self.stdout == "/dev/null":
            stdout = self.stdout
        else:
            stdout = out_dir + "/" + self.stdout
        run(process_args, stdout=open(stdout))

        fname_cfg, fname_beads, fname_filaments = tmp_files
        os.remove(fname_cfg)
        if self.beads_file is None:
            os.remove(fname_beads)
        if self.filaments_file is None:
            os.remove(fname_filaments)

    def run_batch(self, config_template: str, n_threads: int, write_interval: int,
                  param_list: ParamList, labels: List[str],
                  log_level: str='warning', stdout: str="/dev/null",
                  beads_file: str=None, filaments_file: str=None):
        self.config_template = config_template
        self.write_interval = write_interval
        self.labels = labels
        self.log_level = log_level
        self.stdout = stdout
        self.beads_file = beads_file
        self.filaments_file = filaments_file
        pool = Pool(n_threads)
        result = pool.map(self._start_sim, param_list)
        
    def __call__(self, config_template: str, n_threads: int, write_interval: int,
                 param_list: ParamList, labels: List[str],
                 log_level: str='warning', stdout: str="/dev/null",
                 beads_file: str=None, filaments_file: str=None):
        self.run_batch(config_template, n_threads, write_interval,
                       param_list, labels, log_level, stdout,
                       beads_file, filaments_file)
