#! /usr/bin/python3.7
import numpy as np
from typing import Tuple

def collides(coords: np.ndarray, other_coords: np.ndarray,
             bead_diameter: float, box: np.ndarray) -> bool:
    if len(other_coords) == 0:
        return False
    dv = get_minimum_image_vector(other_coords, coords, box)
    distances_squared = (dv**2).sum(1)
    return (distances_squared < bead_diameter**2).any()

def get_minimum_image_vector(v1, v2, simulation_box):
    dv = v1 - v2
    for i in range(len(simulation_box)):
        size_i = simulation_box[i]
        dv[:, i] -= size_i * np.rint(dv[:, i] / size_i)
    return dv        

def rotation_matrix(v: np.ndarray) -> np.ndarray:
    """
    Rotation that maps (1, 0, 0) onto v, wehre |v| = 1.
    """
    theta = np.arccos(v[2])
    phi = np.arccos(v[0]/np.sin(theta))
    gamma = -np.pi/2 + theta
    cos = np.cos
    sin = np.sin
    return np.array([[cos(phi)*cos(gamma), -sin(phi), cos(phi)*sin(gamma)],
                     [sin(phi)*cos(gamma), cos(phi), sin(phi)*sin(gamma)],
                     [-sin(gamma), 0, cos(gamma)]])


def create_filament(n_beads: int,
                    box: np.ndarray,
                    bead_diameter: float,
                    resting_length: float,
                    k_stretch: float,
                    k_bend: float,
                    time_step: float) -> Tuple[np.ndarray, np.ndarray]:
    """
    Create a filament with given parameters.
    """
    coords = np.empty((n_beads, 3))
    links = np.full((n_beads, 3), -1, dtype=int)

    coords[0] = np.random.rand(3) * box
    direction = np.random.randn(3)

    resting_length = resting_length/bead_diameter
    
    if k_stretch < 1e-15:
        sigma_l = resting_length*2
    else:
        sigma_l = np.sqrt(1/k_stretch)
        
    if k_bend < 1e-15:
        sigma_theta = 2*np.pi
    else:
        sigma_theta = np.sqrt(1/k_bend)
        
    i = 1
    while i < n_beads:
        direction = direction / np.sqrt(np.sum(direction**2))
        l = np.random.randn() * sigma_l + 1
        theta = np.random.randn() * sigma_theta
        gamma = np.random.rand() * np.pi
        relative_position = np.array([l * np.cos(theta), 
                                      l * np.sin(theta) * np.cos(gamma), 
                                      l * np.sin(theta) * np.sin(gamma)])
        coords[i] = coords[i-1] + rotation_matrix(direction).dot(relative_position)
        if not collides(coords[i], coords[:i], bead_diameter, box):
            direction = coords[i] - coords[i-1]
            i = i+1

    links[1:, 0] = np.arange(0, n_beads-1)
    links[:-1, 1] = np.arange(1, n_beads)

    return coords, links
    

def write_beads_file(fname, n_beads, coords, links):
    fbeads = open(fname, 'wt')

    for i in range(n_beads):
        fbeads.write("{}\t".format(i))
        fbeads.write("{}\t{}\t{}\t".format(coords[i, 0], coords[i, 1], coords[i, 2]))
        fbeads.write("{}\t{}\t{}\n".format(links[i, 0], links[i, 1], links[i, 2]))
    fbeads.close()

def write_filaments_file(fname, n_beads):
    ffilaments = open(fname, 'wt')
    ffilaments.write("0\t0\t{}\t{}".format(n_beads-1, n_beads))
    ffilaments.close()
