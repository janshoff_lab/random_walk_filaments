import os
import numpy as np

from create_filament import collides, rotation_matrix

class System:

    def __init__(self, reserve_n_beads: int, n_steps: int,
                 rate_nucleation: float, rate_assembly: float,
                 box: np.ndarray, k_bend: float, k_stretch: float,
                 no_nucleation_after: int=None):
        self._rn_handler = _RandomNumberHandler(2000)
        self.coords = np.full((reserve_n_beads, 3), np.nan)
        self.n_steps = n_steps
        self.rate_nucleation = rate_nucleation
        self.rate_assembly = rate_assembly
        self.box = box
        if k_stretch < 1e-15:
            self.sigma_stretch = 2
        else:
            self.sigma_stretch = np.sqrt(1/k_stretch)
        if k_bend < 1e-15:
            self.sigma_theta = 2*np.pi
        else:
            self.sigma_theta = np.sqrt(1/k_bend)
        if no_nucleation_after is None:
            self.no_nucleation_after = self.n_steps
        else:
            self.no_nucleation_after = no_nucleation_after
        
        self.filaments = []
        self.t = 0
        self.nucleation_events = []
        self._draw_nucleation_events()
        self.current_nucleation_event = 0
        self.n_valid = 0

    def _nucleation_happens(self) -> bool:
        if len(self.nucleation_events) <= self.current_nucleation_event:
            return False
        if self.nucleation_events[self.current_nucleation_event] == self.t:
            self.current_nucleation_event += 1
            return True
        return False

    def _add_filament(self):
        start, end = self.n_valid, self.n_valid+1        
        self.coords[start] = self._get_random_location()
        self.n_valid += 1
        direction = self._rn_handler.get_random_direction()
        self.coords[end] = self._get_append_bead_location(self.coords[start],
                                                          direction,
                                                          start)
        self.n_valid += 1
        f = Filament(start, end, self.rate_assembly, self.n_steps, self.t)
        self.filaments.append(f)

    def _get_random_location(self):
        n_trials = 10000
        for i in range(n_trials):
            coords = np.random.rand(3) * self.box
            if not collides(coords, self.coords[:self.n_valid], 1.0, self.box):
                return coords
        msg = "Box too full or bad luck? Could not find an empty space in {} trials".format(
            n_trials)
        raise RuntimeError(msg)

    def _get_append_bead_location(self, start_location: np.ndarray,
                                  direction: np.ndarray, idx_previous: int):
        direction = direction / np.sqrt(np.sum(direction**2))
        n_trials = 10000
        for i in range(n_trials):
            loop_factor = 1 + i/n_trials*5
            l = np.random.randn() * self.sigma_stretch * loop_factor + 1
            theta = np.random.randn() * self.sigma_theta * loop_factor
            gamma = np.random.randn()*np.pi
            relative_position = np.array([l * np.cos(theta),
                                          l * np.sin(theta) * np.cos(gamma),
                                          l * np.sin(theta) * np.sin(gamma)])
            coords = start_location + rotation_matrix(direction).dot(relative_position)
            if not collides(coords,
                            np.vstack([self.coords[:idx_previous],
                                       self.coords[idx_previous+1:self.n_valid]]),
                            1.0, self.box):
                return coords
        msg = "Box too full or bad luck? Could not find an empty space in {} trials".format(
            n_trials)
        raise RuntimeError(msg)

    def _draw_nucleation_events(self):
        self.nucleation_events = self._rn_handler.get_poisson_events(
            self.rate_nucleation, self.no_nucleation_after)

    def run(self, verbose=False):
        n_digits = len(str(self.n_steps))
        percent = self.n_steps//100
        process_str = "creating network ... step {:0" + str(n_digits) + "}/"+str(self.n_steps)
        for t in range(0, self.n_steps):
            if verbose:
                if t % percent == 0:
                    print(process_str.format(t), end='\r')
            self.t = t
            if self._nucleation_happens():
                self._add_filament()
            for f in self.filaments:
                if f.assembly_happens(self.t):
                    self._add_bead(f)
        if verbose:
            print()

    def _add_bead(self, filament):
        prev = self.coords[filament.items[-2]]
        end = self.coords[filament.items[-1]]
        direction = self._get_minimum_image_vector(prev, end)
        new_bead = self._get_append_bead_location(end, direction, filament.items[-1])
        self.coords[self.n_valid] = new_bead
        filament.items.append(self.n_valid)
        self.n_valid += 1

    def _get_minimum_image_vector(self, u, v):
        w = v-u
        m = w - self.box * np.rint(w / self.box)
        return m
        
    def write(self, folder: str):
        if not os.path.exists(folder):
            os.mkdir(folder)
                
        self._write_beads(os.path.join(folder, 'beads.txt'))
        self._write_filaments(os.path.join(folder, 'filaments.txt'))

    def _write_beads(self, fname):
        fp = open(fname, 'wt')
        n_written = 0
        for fil in self.filaments:
            items_in_write_order = []
            for i in range(len(fil.items)):
                if i == 0:
                    prev = -1
                else:
                    prev = n_written - 1
                current = n_written
                if i < len(fil.items)-1:
                    nxt = n_written + 1
                else:
                    nxt = -1
                fp.write("{}\t".format(current))
                r = self.coords[fil.items[i]]
                fp.write("{}\t{}\t{}\t".format(r[0], r[1], r[2]))
                fp.write("{}\t{}\t{}\n".format(prev, nxt, -1))
                n_written += 1
                items_in_write_order.append(current)
            fil.items = items_in_write_order
        fp.close()

    def _write_filaments(self, fname):
        fp = open(fname, 'wt')
        for i, fil in enumerate(self.filaments):
            fp.write("{}\t{}\t{}\t{}\n".format(i,
                                               fil.items[0],
                                               fil.items[-1],
                                               len(fil.items)))
        fp.close()

        
class _RandomNumberHandler:
    
    def __init__(self, n_batch: int):
        self.n_batch = n_batch
        self._random_directions = np.empty((n_batch, 3))
        self._n_used_directions = None
        
        self._generate_random_directions()

    def _generate_random_directions(self):
        """
        Use area-preserving projection from cylinder on a sphere, as described in
        https://math.stackexchange.com/a/44691 by Jim Belk.
        """
        theta = np.random.rand(self.n_batch)*2*np.pi
        z = np.random.rand(self.n_batch)*2 - 1
        self._random_directions[:, 0] = np.sqrt(1-z**2)*np.cos(theta)
        self._random_directions[:, 1] = np.sqrt(1-z**2)*np.sin(theta)
        self._random_directions[:, 2] = z
        self._n_used_directions = 0
        
    def get_random_direction(self):
        if self._n_used_directions >= self.n_batch:
            self._generate_random_directions()
        self._n_used_directions += 1
        return self._random_directions[self._n_used_directions-1]

    @staticmethod
    def get_poisson_events(rate, n_steps) -> np.ndarray:
        events_generated = False
        event_times = np.array([])
        cumsum = 0
        while not events_generated:
            expected_waiting_time = 1 / rate
            expected_n_events = n_steps * expected_waiting_time
            intervals = - np.log(np.random.rand(int(expected_n_events * 1.2))) / rate
            event_times = np.hstack([event_times, np.cumsum(intervals)+cumsum])
            cumsum = event_times[-1]
            if event_times[-1] >= n_steps:
                events_generated = True                            
        return (event_times[event_times < n_steps] + 0.5).astype(int)    

class Filament:

    def __init__(self, first, last, rate, n_steps, current_step):
        self.items=[first, last]
        event_times = _RandomNumberHandler.get_poisson_events(
            rate, n_steps-current_step)
        self.assembly_events = event_times + current_step
        self.current_event = 0

    def assembly_happens(self, current_step) -> bool:
        if len(self.assembly_events) <= self.current_event:
            return False
        if self.assembly_events[self.current_event] == current_step:
            self.current_event += 1
            return True
        return False
        

def create_network(folder: str, n_steps: int,
                   rate_nucleation: float, rate_assembly: float,
                   box: np.ndarray, k_bend: float, k_stretch: float,
                   no_nucleation_after: int):    
    expected_max = int((no_nucleation_after*rate_nucleation * n_steps*rate_assembly)*2)
    # print("expected_max: ", expected_max)
    # expected_n = 10000
    s = System(expected_max, n_steps, rate_nucleation, rate_assembly,
               box, k_bend, k_stretch, no_nucleation_after)
    s.run()
    # print("got:          ", s.n_valid)
    s.write(folder)

if __name__ == "__main__":
    folder = 'network'
    n_steps = int(1e7)
    rate_nucleation = 0.008
    rate_assembly = 0.01
    box = np.array([200, 200, 200])
    k_bend = 23.5
    k_stretch = 20.0
