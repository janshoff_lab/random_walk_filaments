from batch_starter import BatchStarter


def test_create_param_list():

    param_list, labels = BatchStarter.create_param_list(k_stretch=[0.0, 1.0])

    assert 2==len(param_list)
    assert 1==len(labels)
    assert 'k_stretch' in labels
    for i, p in enumerate(param_list):
        assert 2==len(p)
        assert i==p[0]

    param_list, labels = BatchStarter.create_param_list(k_stretch=[0.0, 1.0],
                                                        diffusion_constant=[0.1, 0.375, 0.9])

    assert 6==len(param_list)
    assert 2==len(labels)
    assert 'k_stretch' in labels
    assert 'diffusion_constant' in labels
    for i, p in enumerate(param_list):
        assert 3==len(p)
        assert i==p[0]

    param_list, labels = BatchStarter.create_param_list(k_stretch=[0.0, 1.0],
                                                        diffusion_constant=[0.1, 0.375, 0.9],
                                                        multiplier=5)

    assert 30==len(param_list)
    assert 2==len(labels)
    assert 'k_stretch' in labels
    assert 'diffusion_constant' in labels
    for i, p in enumerate(param_list):
        assert 3==len(p)
        assert i==p[0]

    ## With multiplier set to 5, I expect every block of 5 parameter sets
    # to contain exactly the same parameters, except for the first one,
    # which is the id of the simulation.        
    for i in range(0, 30, 5):
        pslice = param_list[i:i+5]
        for j in range(1, 5):
            p = pslice[j]
            for k in range(1, len(p)):
                assert p[k] == pslice[0][k]
