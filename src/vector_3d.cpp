#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

void Vector3D::add(const Vector3D& v) {
  x += v.x;
  y += v.y;
  z += v.z;
}

void Vector3D::remove(const random_walk_filaments::Vector3D &v) {
  x -= v.x;
  y -= v.y;
  z -= v.z;
}


double Vector3D::norm_squared() const {
  return x*x + y*y + z*z;
}

double Vector3D::norm() const {
  return std::sqrt( norm_squared() );
}

double Vector3D::dot(const Vector3D &v) const {
  return x * v.x + y * v.y + z * v.z;
}

Vector3D Vector3D::scalar_product(double c) const{
  Vector3D result;
  result.x = c * x;
  result.y = c * y;
  result.z = c * z;
  return result;
}

double Vector3D::distance_squared(const Vector3D &v) const {
  Vector3D relative = Vector3D::subtract(v, *this);
  return relative.norm_squared();
}

double Vector3D::distance(const Vector3D &v) const {
  Vector3D relative = Vector3D::subtract(v, *this);
  return relative.norm();
}

bool Vector3D::isnan() const {  
  if (std::isnan(x)) return true;
  if (std::isnan(y)) return true;
  if (std::isnan(z)) return true;
  return false;  
}

/* 
   =============================================================================
   static methods
   =============================================================================
 */

Vector3D Vector3D::sum(const random_walk_filaments::Vector3D &v1,
		       const random_walk_filaments::Vector3D &v2) {
  Vector3D result;
  result.x = v1.x + v2.x;
  result.y = v1.y + v2.y;
  result.z = v1.z + v2.z;
  return result;
}

Vector3D Vector3D::subtract(const random_walk_filaments::Vector3D &v1, const random_walk_filaments::Vector3D &v2) {
  Vector3D result;
  result.x = v1.x - v2.x;
  result.y = v1.y - v2.y;
  result.z = v1.z - v2.z;
  return result;
}

double Vector3D::factor_shortest_distance_to_path(const Vector3D &d,
						  const Vector3D &p) {
  double s_shortest_distance = \
    (p.x * d.x + p.y * d.y + p.z * d.z) / (d.x*d.x + d.y*d.y + d.z*d.z);
  return s_shortest_distance;
}

inline Vector3D Vector3D::operator+(const Vector3D &other) const{
  return Vector3D::sum(*this, other);
}

Vector3D Vector3D::nan() {
  Vector3D v;
  v.x = NAN;
  v.y = NAN;
  v.z = NAN;
  return v;
}

Vector3D Vector3D::minimum_image_vector(const Vector3D &v1, const Vector3D &v2,
					const Vector3D &box) {
  Vector3D mi_v;
  Vector3D v = Vector3D::subtract(v2, v1);
  mi_v.x = v.x - box.x * nearbyint(v.x / box.x);
  mi_v.y = v.y - box.y * nearbyint(v.y / box.y);
  mi_v.z = v.z - box.z * nearbyint(v.z / box.z);
  return mi_v;
}

double Vector3D::minimum_image_distance_squared(const Vector3D &v1,
						       const Vector3D &v2,
						       const Vector3D &box) {
  Vector3D mi_vector = Vector3D::minimum_image_vector(v1, v2, box);
  return mi_vector.norm_squared();
}
