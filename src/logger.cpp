#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

/*
  ==============================================================================
  interface methods
  ==============================================================================
 */

void Logger::log(int level, std::string message) {
  if (log_stream == NULL) return;
  if (level < log_up_to) return;
  std::string current_prefix = "";
  if (prefixes.size() > 0) current_prefix = prefixes[prefixes.size() - 1];
  *log_stream << current_prefix
	      << message
	      << std::endl;
}

void Logger::log(int level, std::ostringstream &message_stream) {
  log(level, message_stream.str());
  message_stream.str("");
  message_stream.clear();
}

void Logger::set_log_prefix(std::string prefix) {
  prefixes.push_back(prefix);
}

void Logger::revert_to_previous_prefix() {
  if (prefixes.size() == 0) return;
  prefixes.erase(prefixes.end() - 1);
}

/*
  ==============================================================================
  set up methods (call directly after initialization)
  ==============================================================================
 */

void Logger::set_log_stream(std::ostream *stream) {
  log_stream = stream;
}

void Logger::set_log_up_to(int level) {
  log_up_to = level;
}
