#include "../include/random_walk_filaments.hpp"
#include <stdexcept>

using namespace random_walk_filaments;

void System::handle_polymerization_events(unsigned int t_current) {
  handle_disassembly_events(t_current);
  handle_assembly_events(t_current);
  handle_nucleation_events(t_current);
}

void System::move_one_step() {
  current_step ++;
  compute_filament_forces();
  random_wrapper->fill_vector_normal(thermal_motion, n_valid_beads, noise_coeff);
  //std::vector<unsigned> index_vector = random_wrapper->generate_shuffled_index_vector(n_valid_beads);
  for ( unsigned i = 0; i < n_valid_beads-1; i++ ) {
    for (unsigned j = i+1; j < n_valid_beads; j++ ) {
      add_harmonic_repulsion(i, j);
    }
  }
  update_positions();
  file_writer->push_bead_states(beads, filaments, current_step, n_valid_beads);
}

void System::move_one_step_with_distance_grid() {
  current_step ++;
  compute_filament_forces();
  random_wrapper->fill_vector_normal(thermal_motion, n_valid_beads, noise_coeff);
  distance_grid->update_cell_map(beads, n_valid_beads);
  std::vector<cell_index_t> cidces = distance_grid->get_cell_indices();  
  for (const cell_index_t& cidx: cidces) handle_pair_interactions(cidx);
  update_positions();
  file_writer->push_bead_states(beads, filaments, current_step, n_valid_beads);
}

void System::handle_pair_interactions(const cell_index_t& cidx) {
  const std::vector<unsigned>& bead_indices = distance_grid->get_bead_indices(cidx);    
  unsigned len = bead_indices.size();
  if (len == 0) return;
  for (unsigned i=0; i < len-1; i++) {
    for (unsigned j=i+1; j < len; j++) {
      add_harmonic_repulsion(bead_indices.at(i), bead_indices.at(j));
    }
  }  
  std::vector<cell_index_t> neighbor_cells = distance_grid->get_forward_neighbors(cidx);    
  for (const cell_index_t& cidx_fwd_neighbor_cell: neighbor_cells) {
    const std::vector<unsigned>& bead_indices_other = distance_grid->get_bead_indices(cidx_fwd_neighbor_cell);
    unsigned len_other = bead_indices_other.size();
    for (unsigned i=0; i < len; i++) {
      for (unsigned k=0; k < len_other; k++) {
        add_harmonic_repulsion(bead_indices.at(i), bead_indices_other.at(k));
      }
    }
  }  
}

void System::add_harmonic_repulsion(unsigned idx1, unsigned idx2) {
  const Bead& b1 = beads.at(idx1);
  const Bead& b2 = beads.at(idx2);
  if (b1.next == idx2 || b1.previous == idx2) return;
  Vector3D v = minimum_image_vector(b1.position, b2.position);
  double r_squared = v.norm_squared();
  if (r_squared > 1.0) return;
  double r = std::sqrt(r_squared);
  double f_abs = 1./r * k_harmonic_repulsion * (r-1);
  Vector3D f = v.scalar_product(f_abs);
  forces.at(idx1).add(f);
  forces.at(idx2).remove(f);
}

void System::compute_filament_forces() {
  for (int i=0; i<n_valid_beads; i++) {
    forces.at(i).x = 0.0;
    forces.at(i).y = 0.0;
    forces.at(i).z = 0.0;    
  }  
  for (int i=0; i<n_valid_beads; i++) {
    Bead &b = beads.at(i);
    Vector3D v_n, v_nn, v_p;
    if (b.next == -1) {
      v_n = Vector3D::nan();
      v_nn = Vector3D::nan();
    } else {
      Bead &next = beads.at(b.next);
      v_n = minimum_image_vector(b.position, next.position);
      if (next.next == -1) {
	v_nn = Vector3D::nan();
      }
      else {
	Bead &next_next = beads.at(next.next);
	v_nn = minimum_image_vector(next.position,
				    next_next.position);
      }
    }
    if (b.previous == -1) {
      v_p = Vector3D::nan();
    } else {
      Bead &prev = beads.at(b.previous);
      v_p = minimum_image_vector(prev.position, b.position);
    }
    Vector3D fwd_stretch = compute_forward_stretch(v_n);
    Vector3D fwd_bend = compute_forward_bend(v_p, v_n, v_nn);
    Vector3D fwd_total = Vector3D::sum(fwd_stretch, fwd_bend);
    forces.at(i).x += fwd_total.x;
    forces.at(i).y += fwd_total.y;
    forces.at(i).z += fwd_total.z;
    if (b.next == -1) continue;    
    forces.at(b.next).x -= fwd_total.x;
    forces.at(b.next).y -= fwd_total.y;
    forces.at(b.next).z -= fwd_total.z;
  }
}

void System::update_positions() {
  for (unsigned i=0; i<n_valid_beads; i++) {
    Vector3D displacement = Vector3D::sum(forces.at(i), thermal_motion.at(i));
    double d = displacement.norm();
    if (d >= 0.5) {
      logger->set_log_prefix("System::update_positions: ");
      std::ostringstream log_stream;
      log_stream << "encountered dangerously large displacement: ||d_" << i << "|| = " << d << std::endl;
      logger->log(Logger::LOG_LEVEL_WARNING, log_stream);
      logger->revert_to_previous_prefix();
    }
    Bead& b = beads.at(i);
    b.position.add(displacement);
  }
}

void System::perform_annealing(unsigned n_steps, double relative_dt, double displacement_cap, bool use_grid) {
    if (n_steps == 0) return;
    double old_k_bend = k_bend;
    double old_k_stretch = k_stretch;
    double old_k_rep = k_harmonic_repulsion;
    double old_noise_coeff = noise_coeff;

    k_bend = k_bend * relative_dt;
    k_stretch = k_stretch * relative_dt;
    k_harmonic_repulsion = k_harmonic_repulsion * relative_dt;
    noise_coeff = noise_coeff * std::sqrt(relative_dt);

    if (use_grid) {
      for (unsigned i=0; i<n_steps; i++) {
        compute_filament_forces();
        random_wrapper->fill_vector_normal(thermal_motion, n_valid_beads, noise_coeff);
        distance_grid->update_cell_map(beads, n_valid_beads);
        std::vector<cell_index_t> cidces = distance_grid->get_cell_indices();
        for (const cell_index_t& cidx: cidces) handle_pair_interactions(cidx);
        update_positions_with_cap(displacement_cap);
      }
    } else {
      for (unsigned i=0; i<n_steps; i++) {
        compute_filament_forces();
        random_wrapper->fill_vector_normal(thermal_motion, n_valid_beads, noise_coeff);
        for ( unsigned i = 0; i < n_valid_beads-1; i++ ) {
          for (unsigned j = i+1; j < n_valid_beads; j++ ) {
            add_harmonic_repulsion(i, j);
          }
        }
        update_positions_with_cap(displacement_cap);
      }
    }

    k_bend = old_k_bend;
    k_stretch = old_k_stretch;
    k_harmonic_repulsion = old_k_rep;
    noise_coeff = old_noise_coeff;
}

void System::update_positions_with_cap(double displacement_cap) {
  for (unsigned i=0; i<n_valid_beads; i++) {
    Vector3D displacement = Vector3D::sum(forces.at(i), thermal_motion.at(i));
    double d = displacement.norm();
    if (d >= displacement_cap) {
      logger->set_log_prefix("System::update_positions: ");
      std::ostringstream log_stream;
      log_stream << "will cap too large displacement: ||d_" << i << "|| = " << d;
      logger->log(Logger::LOG_LEVEL_INFO, log_stream);
      logger->revert_to_previous_prefix();
      displacement = displacement.scalar_product(displacement_cap/d);
    }
    Bead& b = beads.at(i);
    b.position.add(displacement);
  }
}

void System::handle_nucleation_events(unsigned t_current) {
  if (t_current < nucleation_event) return;  
  unsigned fid = create_new_filament();
  // double V_unoccupied = volume_total / volume_single_bead - n_valid_beads;
  // double total_rate = assembly_rate;// * V_unoccupied;
  double total_rate = nucleation_rate;
  if (total_rate <= 0.0) {
    nucleation_event = std::numeric_limits<unsigned>::max();
    return;
  }
  double r = random_wrapper->draw_double_in_range(0.0, 1.0);
  unsigned t_next_nucleation = (unsigned) (-log(r) / total_rate);
  nucleation_event = t_current + t_next_nucleation;
  if (fid != Filament::ID_INVALID) {
    insert_next_assembly_event(t_current, fid);
    insert_next_disassembly_event(t_current, fid);
  }
  handle_nucleation_events(t_current);
}

void System::handle_assembly_events(unsigned t_current) {
  if (assembly_events.size() < 1) return;
  std::multimap<unsigned, unsigned>::iterator it = assembly_events.begin();
  unsigned t_next = (*it).first;
  if (t_current < t_next) return;
  unsigned fid = (*it).second;
  add_bead_to_filament(fid);
  assembly_events.erase(it);
  insert_next_assembly_event(t_current, fid);  
  handle_assembly_events(t_current);
}

void System::insert_next_assembly_event(unsigned t_current, unsigned filament_id) {
  // draw new time stamp for filament
  if (assembly_rate <= 0.0) {
    assembly_events.insert(std::pair<unsigned, unsigned>(std::numeric_limits<unsigned>::max(), filament_id));
    return;
  }
  double r = random_wrapper->draw_double_in_range(0.0, 1.0);
  unsigned t_next_assembly = t_current + (unsigned) (-log(r)/assembly_rate);
  assembly_events.insert(std::pair<unsigned, unsigned>(t_next_assembly, filament_id));
}

void System::insert_next_disassembly_event(unsigned int t_current, unsigned int filament_id) {
  if (disassembly_rate <= 0.0) {
    disassembly_events.insert(std::pair<unsigned, unsigned>(std::numeric_limits<unsigned>::max(),
							    filament_id));
    return;
  }
  double r = random_wrapper->draw_double_in_range(0.0, 1.0);
  unsigned t_next_disassembly = t_current + (unsigned) (-log(r)/disassembly_rate);
  disassembly_events.insert(std::pair<unsigned, unsigned>(t_next_disassembly, filament_id));
}

void System::handle_disassembly_events(unsigned int t_current) {
  if (disassembly_events.size() < 1) {
    return;
  }
  std::multimap<unsigned, unsigned>::iterator it = disassembly_events.begin();
  unsigned t_next = (*it).first;
  if (t_current < t_next) {
    return;
  }
  unsigned fid = (*it).second;
  unsigned n_filaments = filaments.size();
  // std::ostringstream log_stream;
  // log_stream << "will remove bead from filament with id " << fid;
  // logger->log(Logger::LOG_LEVEL_DEBUG, log_stream);
  remove_bead_from_filament(fid);
  disassembly_events.erase(it);
  if (filaments.size() == n_filaments) {
    // logger->log(Logger::LOG_LEVEL_DEBUG, "filament did not disapper");
    // draw new time stamp for filament    
    insert_next_disassembly_event(t_current, fid);
  } else {
    // logger->log(Logger::LOG_LEVEL_DEBUG, "filament disappeared!");
    // log_stream << "disassembly_events = {" << std::endl;
    //for (auto it_log=disassembly_events.begin(); it_log!=disassembly_events.end(); ++it_log) {
    //  log_stream << (*it_log).first << ", " << (*it_log).second << std::endl;
    //}
    //log_stream << "}";
    //logger->log(Logger::LOG_LEVEL_DEBUG, log_stream);
    // find next assembly event of disassemled filament, and erase it
    const std::multimap<unsigned, unsigned>::const_iterator it_a =\
      std::find_if(assembly_events.begin(), assembly_events.end(),
		   [fid](const std::pair<unsigned, unsigned> &p) -> bool {return p.second == fid;});
    if (it_a != assembly_events.end()) assembly_events.erase(it_a);
  }
  handle_disassembly_events(t_current);
  
  // logger->revert_to_previous_prefix();
}

Bead System::bead_line_to_bead_object(std::istringstream &line_stream) {
  Bead b;
  line_stream >> b.id;
  line_stream >> b.position.x;
  line_stream >> b.position.y;
  line_stream >> b.position.z;
  line_stream >> b.previous;
  line_stream >> b.next;
  line_stream >> b.cross_filament;
  if ((b.previous == -1) & (b.next == -1)) {
    throw std::runtime_error("can't deduce bead state from line!");    
  } else if (b.previous == -1) {
    b.state = BeadState::FilamentBegin;
  } else if (b.next == -1) {
    b.state = BeadState::FilamentEnd;
  } else {
    b.state = BeadState::FilamentInterior;
  }
  return b;
}

Filament System::filament_line_to_filament_object(std::istringstream &line_stream) {
  Filament f;
  line_stream >> f.id;
  line_stream >> f.first;
  line_stream >> f.last;
  line_stream >> f.n_beads_total;
  return f;
}

void System::initialize_beads_via_files(std::string path_to_initial_coords,
					std::string path_to_initial_filaments) {
  if (nucleation_rate <= 0) nucleation_event = std::numeric_limits<unsigned>::max();  
  else {
    double r = random_wrapper->draw_double_in_range(0.0, 1.0);
    nucleation_event = (unsigned) (-log(r) / nucleation_rate);
  }
  
  // parse coordinates into vector of doubles
  std::ifstream beads_file_stream(path_to_initial_coords);  
  std::string line;
  n_valid_beads = 0;  
  unsigned i = 0;  
  while(std::getline(beads_file_stream, line)) {
    std::istringstream line_stream(line);
    Bead b_new = bead_line_to_bead_object(line_stream);
    Bead &b = beads.at(i);
    b.copy_properties(b_new);    
    n_valid_beads++;
    if (b.id+1 > b.count_total) b.count_total = b.id+1;
    i++;      
  }
  
  filaments.clear();
  std::ifstream filaments_file_stream(path_to_initial_filaments);
  
  while(std::getline(filaments_file_stream, line)) {
    std::istringstream line_stream(line);
    Filament f = filament_line_to_filament_object(line_stream);
    filaments.push_back(f);
    if (f.id+1 > f.count_total) f.count_total = f.id+1;
    insert_next_assembly_event(0, f.id);
    insert_next_disassembly_event(0, f.id);
  }
}

void System::write_final_states(std::string output_dir) {
  std::ofstream ofs_beads(output_dir + "/beads_final.txt");
  std::ofstream ofs_filaments(output_dir + "/filaments_final.txt");

  ofs_beads << std::setprecision(12);

  for (std::vector<Bead>::const_iterator it = beads.begin(); it != beads.begin() + n_valid_beads; it++) {
    const Bead &b = *it;
    Vector3D v = b.position;
    ofs_beads << b.id << "\t" << v.x << "\t" << v.y << "\t" << v.z << "\t"
	      << b.previous << "\t" << b.next << "\t" << b.cross_filament << std::endl;
  }

  for (const Filament &f: filaments) {
    ofs_filaments << f.id << "\t" << f.first << "\t" << f.last << "\t"
		  << f.n_beads_total << std::endl;
  }
  ofs_beads.close();
  ofs_filaments.close();
}

Vector3D System::compute_forward_stretch(const Vector3D &v_n) {
  double l_n = v_n.norm();
  double factor_n = 1. - 1. / l_n;
  Vector3D f_stretch;
  f_stretch.x = factor_n * v_n.x;
  f_stretch.y = factor_n * v_n.y;
  f_stretch.z = factor_n * v_n.z;
  if (f_stretch.isnan()){
    f_stretch.x = 0.;
    f_stretch.y = 0.;
    f_stretch.z = 0.;
  }
  return f_stretch.scalar_product(k_stretch);
}

Vector3D System::compute_forward_bend(const Vector3D &v_p,
				      const Vector3D &v_n,
				      const Vector3D &v_nn) {
  Vector3D f_bend;
  double l_p = v_p.norm();
  double l_n = v_n.norm();
  double l_nn = v_nn.norm();
  
  double vv_c = v_p.dot(v_n);
  double ll_c_inverse = 1. / (l_p * l_n);

  double vv_n = v_n.dot(v_nn);
  double ll_n_inverse = 1. / (l_n * l_nn);

  double cos_c = vv_c * ll_c_inverse;
  double cos_n = vv_n * ll_n_inverse;

  double sin_c = sqrt(1 - cos_c*cos_c);
  double sin_n = sqrt(1 - cos_n*cos_n);

  double alpha_c = acos(cos_c);
  double alpha_n = acos(cos_n);

  Vector3D term_c_1 = v_p.scalar_product(ll_c_inverse);
  Vector3D term_c_2 = v_n.scalar_product(cos_c/l_n/l_n);
  Vector3D term_c = Vector3D::subtract(term_c_2,
				       term_c_1).scalar_product(alpha_c/sin_c);
  if (term_c.isnan()) {
    term_c.x = 0.0;
    term_c.y = 0.0;
    term_c.z = 0.0;
  }

  // theta/sin*(-v[1:]/l_[:-1]/l_[1:] + v[:-1]*cos/l_[:-1]**2)
  Vector3D term_n_1 = v_nn.scalar_product(ll_n_inverse);
  Vector3D term_n_2 = v_n.scalar_product(cos_n/l_n/l_n);
  Vector3D term_n = Vector3D::subtract(term_n_2,
				       term_n_1).scalar_product(alpha_n/sin_n);
  if (term_n.isnan()) {
    term_n.x = 0.0;
    term_n.y = 0.0;
    term_n.z = 0.0;
  }
  
  Vector3D f_forward = Vector3D::sum(term_n, term_c).scalar_product(k_bend);
  return f_forward;
}

/* 
   =============================================================================
   SETTER 
   =============================================================================
*/
void System::set_box_size(Vector3D box_size) {
  this->box_size = box_size;
  link_collision_handler();
}

void System::link_collision_handler() {
  collision_handler.beads = &beads;
  collision_handler.box   = &box_size;
}

void System::set_noise_coeff(double c) {
  noise_coeff = c;
}

void System::set_harmonic_repulsion_coefficient(double k) {
  k_harmonic_repulsion = k;
}
void System::set_k_stretch(double k_s) {
  k_stretch = k_s;
}

void System::set_k_bend(double k_b) {
  k_bend = k_b;
}

void System::set_nucleation_rate(double r) {
  nucleation_rate = r;
}

void System::set_assembly_rate(double r) {
  assembly_rate = r;
}

void System::set_disassembly_rate(double r) {
  disassembly_rate = r;
}

void System::set_bead_file_writer(BeadFileWriter *writer) {
  file_writer = writer;
  file_writer->push_bead_states(beads, filaments, current_step, n_valid_beads);
}

void System::set_logger(Logger* logger_) {
  logger = logger_;
  collision_handler.logger = logger_;
}

void System::set_random_wrapper(RandomWrapper *wrapper) {
  random_wrapper = wrapper;
}

void System::set_distance_grid(DistanceGrid *dg) {
  distance_grid = dg;
}

/* 
   =============================================================================
   GETTER 
   =============================================================================
*/

Vector3D System::get_box_size() {
  return box_size;
}

unsigned System::get_number_of_beads() {
  return beads.size();
}

double System::get_k_stretch() {
  return k_stretch;
}

double System::get_k_bend() {
  return k_bend;
}
