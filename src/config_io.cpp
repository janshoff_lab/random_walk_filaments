#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

void ConfigIO::add_box_size(nlohmann::json &j, random_walk_filaments::Vector3D box) {
  j["box_size"] = nlohmann::json::object();
  j["box_size"]["x"] = box.x;
  j["box_size"]["y"] = box.y;
  j["box_size"]["z"] = box.z;
}

void ConfigIO::add_annealing(nlohmann::json &j, unsigned n_steps,
                             double relative_dt, double displacement_cap) {
  j["annealing"] = nlohmann::json::object();
  j["annealing"]["n_steps"] = n_steps;
  j["annealing"]["relative_dt"] = relative_dt;
  j["annealing"]["displacement_cap"] = displacement_cap;
}

void ConfigIO::add_random_seed(nlohmann::json &j, int seed) {
  j["random_seed"] = seed;
}

void ConfigIO::add_n_beads(nlohmann::json &j, unsigned int n_beads) {
  j["n_beads"] = n_beads;
}

void ConfigIO::add_n_filaments(nlohmann::json &j, unsigned int n_filaments) {
  j["n_filaments"] = n_filaments;
}

void ConfigIO::add_n_steps(nlohmann::json &j, unsigned int n_steps) {
  j["n_steps"] = n_steps;
}

void ConfigIO::add_k_bend(nlohmann::json &j, double k_bend) {
  j["k_bend"] = k_bend;
}

void ConfigIO::add_k_harmonic_repulsion(nlohmann::json &j, double k) {
  j["k_harmonic_repulsion"] = k;
}

void ConfigIO::add_k_stretch(nlohmann::json &j, double k_stretch) {
  j["k_stretch"] = k_stretch;
}

void ConfigIO::add_version(nlohmann::json &j, std::string v) {
  j["VERSION"] = v;
}

void ConfigIO::add_date(nlohmann::json &j, std::string date_string) {
  j["date"] = date_string;
}

void ConfigIO::add_nucleation_rate(nlohmann::json &j, double rate) {
  j["nucleation_rate"] = rate;
}

void ConfigIO::add_assembly_rate(nlohmann::json &j, double rate) {
  j["assembly_rate"] = rate;
}

void ConfigIO::add_disassembly_rate(nlohmann::json &j, double rate) {
  j["disassembly_rate"] = rate;
}

void ConfigIO::add_n_cells_for_distance_grid(nlohmann::json &j, cell_index_t n_cells) {
  j["n_cells_for_distance_grid"] = n_cells;
}

void ConfigIO::add_time_step(nlohmann::json &j, double dt) {
  j["time_step"] = dt;
}

Vector3D ConfigIO::get_box_size(const nlohmann::json &j) {
  Vector3D box;
  box.x = j["box_size"]["x"].get<double>();
  box.y = j["box_size"]["y"].get<double>();
  box.z = j["box_size"]["z"].get<double>();
  return box;
}

void ConfigIO::get_annealing(const nlohmann::json &j, unsigned &n_steps,
                             double &relative_dt, double &displacement_cap) {
  std::cout << "in get_annealing" << std::endl;
  if (j.find("annealing") == j.end()) {
    std::cout << "key not found!" << std::endl;
    n_steps = 0;
    return;
  }
  n_steps = j["annealing"]["n_steps"].get<unsigned>();
  relative_dt = j["annealing"]["relative_dt"].get<double>();
  displacement_cap = j["annealing"]["displacement_cap"].get<double>();
  std::cout << "found everything, exit function" << std::endl;
}

int ConfigIO::get_random_seed(const nlohmann::json &j) {
  return j["random_seed"].get<int>();
}

unsigned ConfigIO::get_n_beads(const nlohmann::json &j) {
  return j["n_beads"].get<unsigned>();
}

unsigned ConfigIO::get_n_filaments(const nlohmann::json &j) {
  return j["n_filaments"].get<unsigned>();
}


unsigned ConfigIO::get_n_steps(const nlohmann::json &j) {
  return j["n_steps"].get<unsigned>();
}

double ConfigIO::get_k_bend(const nlohmann::json &j) {
  return j["k_bend"].get<double>();
}

double ConfigIO::get_k_harmonic_repulsion(const nlohmann::json &j) {
  return j["k_harmonic_repulsion"].get<double>();
}

double ConfigIO::get_k_stretch(const nlohmann::json &j) {
  return j["k_stretch"].get<double>();
}

double ConfigIO::get_nucleation_rate(const nlohmann::json &j) {
  return j["nucleation_rate"].get<double>();
}

double ConfigIO::get_assembly_rate(const nlohmann::json &j) {
  return j["assembly_rate"].get<double>();
}

double ConfigIO::get_disassembly_rate(const nlohmann::json &j) {
  return j["disassembly_rate"].get<double>();
}

std::string ConfigIO::get_version(const nlohmann::json &j) {
  return j["VERSION"].get<std::string>();
}

cell_index_t ConfigIO::get_n_cells_for_distance_grid(const nlohmann::json &j) {
  return j["n_cells_for_distance_grid"].get<cell_index_t>();
}

double ConfigIO::get_time_step(const nlohmann::json &j) {
  return j["time_step"].get<double>();
}
