#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

std::multimap<double, unsigned> CollisionHandler::check_for_beads_in_path(unsigned int index_of_current_bead,
									  const Vector3D &position_of_current_bead,
									  const Vector3D &displacement,
									  const std::vector<unsigned> &bidces) {
  std::multimap<double, unsigned> beads_in_path;
  double relative_diameter = 1.0 / displacement.norm();
  for (unsigned i: bidces) {
    if (i == index_of_current_bead) continue;
    if (beads->at(i).previous == index_of_current_bead) continue;
    if (beads->at(i).next     == index_of_current_bead) continue;
    Vector3D position_compare_bead = beads->at(i).position;
    Vector3D periodic_distance = Vector3D::minimum_image_vector(position_of_current_bead,
								position_compare_bead,
								*box);


    double factor_shortest_distance_to_path = \
      Vector3D::factor_shortest_distance_to_path(displacement,
						 periodic_distance);
    if (factor_shortest_distance_to_path < 0.0) continue;
    if (factor_shortest_distance_to_path > 1.0 + relative_diameter) continue;    
    Vector3D position_shortest_distance = \
      Vector3D::sum(position_of_current_bead,
		    displacement.scalar_product(factor_shortest_distance_to_path));

    double distance_squared = Vector3D::minimum_image_distance_squared(position_shortest_distance,
								       position_compare_bead,
								       *box);

    if (distance_squared > 1.0) continue;
    double factor_diameter_distance = \
      compute_factor_on_path_for_bead_diameter_distance(displacement.norm_squared(),
							distance_squared,
							factor_shortest_distance_to_path);
    if (factor_diameter_distance < -0.001) continue;
    if (factor_diameter_distance > 1.0) continue;
    beads_in_path.insert(std::pair<double, unsigned>(factor_diameter_distance, i));
  }
  return beads_in_path;
}

double CollisionHandler::compute_factor_on_path_for_bead_diameter_distance(double length_of_path_squared,
									   double distance_to_path_squared,
									   double factor_shortest_distance) {
  double k_squared = 1.0 - distance_to_path_squared;
  return factor_shortest_distance - std::sqrt(k_squared / length_of_path_squared);
}  
