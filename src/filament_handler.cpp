#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

void FilamentHandler::add_bead_to_filament(unsigned id, BeadState which_end) {
  for (Filament &f: filaments) {
    if (f.id != id) continue;
    Bead &b = beads.at(n_valid_beads);
    if (which_end == BeadState::FilamentEnd) {
      Bead &bead_p = beads.at(f.last);
      Bead &bead_pp = beads.at(bead_p.previous);
      b.position = get_new_bead_location(bead_p.position, bead_pp.position);
      if (b.position.isnan()) return;
      bead_p.next = n_valid_beads;
      b.previous = f.last;
      b.next = -1;
      f.last = n_valid_beads;
      b.state = BeadState::FilamentEnd;
      bead_p.state = BeadState::FilamentInterior;
    } else {
      Bead &bead_p = beads.at(f.first);
      Bead &bead_pp = beads.at(bead_p.next);
      b.position = get_new_bead_location(bead_p.position, bead_pp.position);
      if (b.position.isnan()) return;
      bead_p.previous = n_valid_beads;
      b.next = f.first;
      b.previous = -1;
      f.first = n_valid_beads;
      b.state = BeadState::FilamentBegin;
      bead_p.state = BeadState::FilamentInterior;
    }
    f.n_beads_total ++;
    n_valid_beads ++;
    b.id = b.count_total;
    b.count_total++;
    return;
  }
  throw std::runtime_error("No Filament with that id!");
}

Vector3D FilamentHandler::get_new_bead_location(Vector3D location_prev, Vector3D location_prev_prev) {
  unsigned max_attempts = 500;
  bool collision;
  Vector3D location;
  for (unsigned i = 0; i < max_attempts; i++) {
    Vector3D diff = minimum_image_vector(location_prev_prev, location_prev);
    Vector3D direction = diff.scalar_product(1./diff.norm());
    double length = random_wrapper->draw_normal(1, new_segment_length_std);
    location = Vector3D::sum(location_prev, direction.scalar_product(length));
    Vector3D noise = random_wrapper->draw_random_unit_vector().scalar_product((i+5)*0.01);
    location = Vector3D::sum(location, noise);
    collision = check_for_collision(location);
    if (!collision) break;
  }
  if (collision) return Vector3D::nan();
  return location;
}

bool FilamentHandler::check_for_collision(const Vector3D &v) {
  for (unsigned i = 0; i < n_valid_beads; i++) {
    Vector3D vi = beads.at(i).position;
    if (minimum_image_distance_squared(v, vi) < 1.0) return true;
  }
  return false;
}

void FilamentHandler::reserve_space_for_beads_and_filaments(uint32_t space_beads,
							    uint16_t space_filaments) {
  beads.reserve(space_beads);
  for (uint32_t i = 0; i < space_beads; i++) {
    Bead b;
    b.position = Vector3D::nan();
    b.state = BeadState::Free;
    beads.push_back(b);
  }
  filaments.reserve(space_filaments);      
}

unsigned FilamentHandler::create_new_filament() {
  Vector3D location1;
  unsigned max_attempts = 500;
  bool collision;
  for (unsigned i = 0; i < max_attempts; i++) {
    location1.x = random_wrapper->draw_double_in_range(0.0, box_size.x);
    location1.y = random_wrapper->draw_double_in_range(0.0, box_size.y);
    location1.z = random_wrapper->draw_double_in_range(0.0, box_size.z);
    collision = check_for_collision(location1);
    if (!collision) break;
  }
  if (collision) return Filament::ID_INVALID;  

  Vector3D random_direction = Vector3D::sum(location1, random_wrapper->draw_random_unit_vector());
  Vector3D location2 = get_new_bead_location(location1, random_direction);
  if (location2.isnan()) return Filament::ID_INVALID;

  Filament f;
  f.id = f.count_total;
  f.count_total ++;  
  
  Bead &b1 = beads.at(n_valid_beads);
  f.first = n_valid_beads;
  b1.state = BeadState::FilamentBegin;
  n_valid_beads++;
  b1.next = n_valid_beads;
  b1.previous = -1;
  b1.id = b1.count_total;
  b1.count_total++;
  
  Bead &b2 = beads.at(n_valid_beads);
  f.last = n_valid_beads;
  b2.state = BeadState::FilamentEnd;
  b2.previous = n_valid_beads-1;
  b2.next = -1;
  n_valid_beads++;
  b2.id = b2.count_total;
  b2.count_total++;
  
  b1.position = location1;
  b2.position = location2;

  f.n_beads_total = 2;

  filaments.push_back(f);
  return f.id;
}

Vector3D FilamentHandler::minimum_image_vector(const Vector3D &v1, const Vector3D &v2) {
  return Vector3D::minimum_image_vector(v1, v2, box_size);
}

double FilamentHandler::minimum_image_distance_squared(const random_walk_filaments::Vector3D &v1, const random_walk_filaments::Vector3D &v2) {  
  return Vector3D::minimum_image_distance_squared(v1, v2, box_size);
}

void FilamentHandler::remove_bead_from_filament(unsigned id, BeadState which_end) {
  int idx_filament = -1;
  for (unsigned i = 0; i < filaments.size(); i++) {
    Filament &f = filaments.at(i);
    if (f.id != id) continue;
    idx_filament = i;
    break;
  }
  if (idx_filament == -1) throw std::runtime_error("No Filament with that id!");
 
  Filament &f = filaments.at(idx_filament);
    
  unsigned idx_bead_to_remove;
  unsigned idx_last_valid_bead = n_valid_beads - 1;
  if  (which_end == BeadState::FilamentEnd) {
    idx_bead_to_remove = f.last;
    Bead &b = beads.at(idx_bead_to_remove);
    Bead &b_linking_to_b = beads.at(b.previous);

    b_linking_to_b.next = -1;
    b_linking_to_b.state = BeadState::FilamentEnd;
    f.last = b.previous;
    
    Bead &b_last_valid = beads.at(idx_last_valid_bead);
    b.copy_properties(b_last_valid);    
  } else {
    idx_bead_to_remove = f.first;
    Bead &b = beads.at(idx_bead_to_remove);
    Bead &b_next = beads.at(b.next);
    b_next.previous = -1;
    b_next.state = BeadState::FilamentBegin;
    f.first = b.next;
    

    Bead &b_last_valid = beads.at(idx_last_valid_bead);
    b.copy_properties(b_last_valid);
  }
  f.n_beads_total --;
  n_valid_beads --;
  fix_links_to_copied_bead(idx_bead_to_remove, idx_last_valid_bead);

  if (f.n_beads_total == 1) {
    int idx_remaining;
    if (which_end == BeadState::FilamentBegin) {
      idx_remaining = f.last;
      if (idx_remaining == n_valid_beads) {
        idx_remaining = f.first;
      }
    } else {
      idx_remaining = f.first;
      if (idx_remaining == n_valid_beads) {
        idx_remaining == f.last;
      }
    }
    // case: complete filament needs to be deleted
    if ((n_valid_beads != 1) && (idx_remaining != (n_valid_beads - 1))) {
      Bead &b = beads.at(idx_remaining);
      b.copy_properties(beads.at(n_valid_beads - 1));
      fix_links_to_copied_bead(idx_remaining, n_valid_beads - 1);
    }
    n_valid_beads--;
    f.copy_properties(filaments.at(filaments.size()-1));
    filaments.pop_back();
  }
}

void FilamentHandler::fix_links_to_copied_bead(unsigned new_idx, unsigned old_idx) {
  Bead &b = beads.at(new_idx);
  if (b.state == BeadState::FilamentEnd) {
    // case: has no next bead -> next=-1
    for (Filament &f: filaments) {
      if (f.last != old_idx) continue;
      f.last = new_idx;      
      break;
    }
  } else {
    // case: has next bead
    if (b.next != -1) {  
      Bead &b_next = beads.at(b.next);
      b_next.previous = new_idx;
    }
  }
  
  if (b.state == BeadState::FilamentBegin) {
    // case: has no previous bead -> previous=-1
    for (Filament &f: filaments) {
      if (f.first != old_idx) continue;
      f.first = new_idx;
      break;
    }
  } else {
    if (b.previous != -1) {
      Bead &b_previous = beads.at(b.previous);
      b_previous.next = new_idx;
    }
  }
}
