#include <stdexcept>
#include <iostream>
#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

void DistanceGrid::update_cell_map(const std::vector<Bead> &beads, unsigned n_valid_beads) {
  clear_cell_map();
  for (unsigned j=0; j < n_valid_beads; j++) {
    add_bead(j, beads.at(j).position);
  }
}

void DistanceGrid::create_empty_cell_map() {
  cell_map_t empty_cell_map;
  for (const cell_index_t& cidx: cell_indices) {
    std::vector<unsigned> v;
    empty_cell_map.insert(std::pair<cell_index_t, std::vector<unsigned>>(cidx, v));
    //cell_map.insert(std::pair<cell_index_t, std::vector<unsigned>>(cidx, v));
  }
  cell_map = empty_cell_map;
}

void DistanceGrid::clear_cell_map() {
  for (const cell_index_t& cidx: cell_indices) {
    cell_map.at(cidx).clear();
  }
}

void DistanceGrid::create_full_cell_indices_vector() {  
  cell_indices.reserve(n_cells[0] * n_cells[1] * n_cells[2]);
  for (unsigned i = 0; i < n_cells[0]; i++) {
    for (unsigned j = 0; j < n_cells[1]; j++) {
      for (unsigned k = 0; k < n_cells[2]; k++) {
	cell_index_t idx = {i, j, k};
	cell_indices.push_back(idx);
      }
    }
  }
}

void DistanceGrid::add_bead(unsigned bidx, Vector3D coords) {  
  cell_index_t cidx = coords_to_cidx(coords);
  /*
  std::cout << "cidx = ("
	    << cidx[0] << ", "
	    << cidx[1] << ", "
	    << cidx[2] << ")" << std::endl;
  std::cout << "coords = ("
	    << coords.x << ", "
	    << coords.y << ", "
	    << coords.z << ")" << std::endl;
  */
  const cell_map_t::iterator& cell = cell_map.find(cidx);
  cell->second.push_back(bidx);
}

cell_index_t DistanceGrid::coords_to_cidx(Vector3D coords) {
  unsigned i, j, k;
  Vector3D minimum_image_v;
  minimum_image_v.x = coords.x - box.x * std::floor(coords.x / box.x);
  minimum_image_v.y = coords.y - box.y * std::floor(coords.y / box.y);
  minimum_image_v.z = coords.z - box.z * std::floor(coords.z / box.z);

  i = (unsigned) (minimum_image_v.x / cell_length.x);
  j = (unsigned) (minimum_image_v.y / cell_length.y);
  k = (unsigned) (minimum_image_v.z / cell_length.z);
  cell_index_t cidx = {i, j, k};
  return cidx;
}

const std::vector<unsigned>& DistanceGrid::get_bead_indices(const cell_index_t& cidx) {
  const cell_map_t::iterator& cell = cell_map.find(cidx);
  if (cell == cell_map.end()) throw std::runtime_error("invalid cell index!");
  return cell->second;
}

std::vector<cell_index_t> DistanceGrid::get_forward_neighbors(const cell_index_t& cidx) {
  std::vector<cell_index_t> neighbor_list;
  neighbor_list.reserve(13);
  int x = cidx[0];
  int y = cidx[1];
  int z = cidx[2];

  int xi, yi, zi;

  yi = y;
  xi = x;
  zi = (z + 1);
  {
    cell_index_t current_cell_index;
    current_cell_index[0] = xi;
    current_cell_index[1] = yi;
    current_cell_index[2] = get_valid_cell_index(zi, n_cells[2]);
    neighbor_list.push_back(current_cell_index);
  }

  xi = get_valid_cell_index(x+1, n_cells[0]); 
  for (zi = z - 1; zi <= z + 1; zi++) {
    cell_index_t current_cell_index;
    current_cell_index[0] = xi;
    current_cell_index[1] = yi;
    current_cell_index[2] = get_valid_cell_index(zi, n_cells[2]);
    neighbor_list.push_back(current_cell_index);      
  }      

  yi = get_valid_cell_index(y + 1, n_cells[1]);
  for (xi = x - 1; xi <= x + 1; xi++) {    
    for (zi = z - 1; zi <= z + 1; zi++) {
      cell_index_t current_cell_index;
      current_cell_index[0] = get_valid_cell_index(xi, n_cells[0]);
      current_cell_index[1] = yi;
      current_cell_index[2] = get_valid_cell_index(zi, n_cells[2]);
      neighbor_list.push_back(current_cell_index);      
    }
  }
  return neighbor_list;
}

unsigned DistanceGrid::get_valid_cell_index(int idx, unsigned max_idx) {
  if (idx >= (int) max_idx) return (unsigned) idx-max_idx;
  else if (idx < 0) return (unsigned)  idx+max_idx;
  else return (unsigned) idx;
}

const std::vector<cell_index_t>& DistanceGrid::get_cell_indices() {
  return cell_indices;
}

void DistanceGrid::set_random_wrapper(RandomWrapper *rw) {
  random_wrapper = rw;
}
