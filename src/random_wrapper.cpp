#include <stdexcept>
#include <algorithm>
#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

void RandomWrapper::initialize() {
  random_engine.seed(seed_value);  
}

unsigned RandomWrapper::draw_uint() {
  return uint_dist(random_engine);
}

double RandomWrapper::draw_double_in_range(double low, double high)  {
  if (low > high) {    
    std::invalid_argument("random::draw_double_in_range: low > high!");
  } if (low == high) {
    return low;
  }
  unsigned int r_int = draw_uint();
  double r = (double) r_int / (double) RW_UINT_MAX;
  r = r * (high - low) + low;
  return r;
}

double RandomWrapper::draw_normal(double mean, double std) {
  return normal_dist(random_engine)*std + mean;
}

void RandomWrapper::fill_vector_normal(std::vector<Vector3D> &v, unsigned n,
				       double std)  {
  for (unsigned i=0; i<n; i++) {
    Vector3D &vi = v.at(i);
    vi.x = normal_dist(random_engine)*std;
    vi.y = normal_dist(random_engine)*std;
    vi.z = normal_dist(random_engine)*std;    
  }
}

Vector3D RandomWrapper::draw_random_unit_vector()  {
  Vector3D v;
  v.x = draw_double_in_range(-1., 1.);
  v.y = draw_double_in_range(-1., 1.);
  v.z = draw_double_in_range(-1., 1.);
  double length = v.norm();
  if ( length > 1. ) {
    return draw_random_unit_vector();
  }
  return v.scalar_product(1./length);
}

void RandomWrapper::set_seed(unsigned int seed) {
  seed_value = seed;
}

unsigned RandomWrapper::get_seed() const {
  return seed_value;
}

std::vector<unsigned> RandomWrapper::generate_shuffled_index_vector(unsigned int size)  {
  std::vector<unsigned> v;
  v.reserve(size);
  for (unsigned i = 0; i < size; i++) {
    v.push_back(i);
  }
  std::shuffle(v.begin(), v.end(), random_engine);
  return v;
}
