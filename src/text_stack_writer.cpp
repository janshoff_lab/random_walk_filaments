#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

TextStackWriter::TextStackWriter(std::string output_dir_, unsigned write_interval_,
				 unsigned n_time_steps_):
  output_dir(output_dir_)
{
  n_time_steps = n_time_steps_;
  write_interval = write_interval_;
  if (write_interval_ == 0) return;
  create_output_format_description();
  ofs_beads = new std::ofstream(output_dir + "/" + FILENAME_BEADS);
  *ofs_beads << std::setprecision(12);
  ofs_filaments = new std::ofstream(output_dir + "/" + FILENAME_FILAMENTS);  
}

TextStackWriter::~TextStackWriter() {
  if (ofs_beads != NULL) ofs_beads->close();
  if (ofs_filaments != NULL) ofs_filaments->close();
}

void TextStackWriter::push_bead_states(const std::vector<Bead> &beads,
				       const std::vector<Filament> &filaments,
				       unsigned int step,
				       unsigned n_valid_beads) {
  if (write_interval == 0) return;
  if (!do_write(step)) return;
  write_beads(beads, n_valid_beads, step);
  write_filaments(filaments, step);
}

void TextStackWriter::write_beads(const std::vector<Bead> &beads,
				  unsigned n_valid_beads, unsigned step) {
  *ofs_beads << "t=" << step << "\tN=" << n_valid_beads << "\n";
  for (std::vector<Bead>::const_iterator it = beads.begin(); it != beads.begin() + n_valid_beads; it++) {
    const Bead &b = *it;
    Vector3D v = b.position;
    *ofs_beads << b.id << "\t" << v.x << "\t" << v.y << "\t" << v.z << "\t"
	       << b.previous << "\t" << b.next << "\t" << b.cross_filament << "\n";
  }
  *ofs_beads << std::flush;
}

void TextStackWriter::write_filaments(const std::vector<Filament> &filaments,
				      unsigned time_step) {
  *ofs_filaments << "t=" << time_step << "\tN=" << filaments.size() << "\n";
  for (const Filament &f: filaments) {
    *ofs_filaments << f.id << "\t" << f.first << "\t" << f.last << "\t"
		   << f.n_beads_total << "\n";
  }
  *ofs_filaments << std::flush;
}

bool TextStackWriter::do_write(unsigned t) {
  if (t % write_interval == 0) return true;
  return false;
}

void TextStackWriter::create_output_format_description() {
  std::ofstream description_stream(output_dir + "/" + FORMAT_DESCRIPTION_FILE);
  description_stream << "# random_walk_filaments " << VERSION << std::endl
		     << "# text stacks" << std::endl
		     << "All files have the following structure:" << std::endl
		     << " - data is not written for every time step; rather the write "
		     << " frequency depends on your configuration" << std::endl
		     << " - when data of a time step is written to file, a new block"
		     << " is added to the output files" << std::endl
		     << " - a block starts with 't=<time_step>\tN=<number_of_lins>'" << std::endl
		     << " - followed by N lines containing the data" << std::endl << std::endl
		     << "# Beads" << std::endl
		     << "coordinates and links of beads in file beads.txt."
		     << std::endl
		     << " - one line per bead:" << std::endl
		     << "   - first a single integer: unique id of the bead" << std::endl
		     << "   - three floats = coordinates (x, y, z) " << std::endl
    		     << "   - three integers = links = indices of beads this bead is connected to (previous, next, cross_filament) " << std::endl << std::endl
		     << "# Filaments" << std::endl
		     << "filament info stored in file filaments.txt" << std::endl
		     << " - one line per filament"
		     << std::endl;
  description_stream.close();
}
