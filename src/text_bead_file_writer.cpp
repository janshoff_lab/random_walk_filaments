#include <fstream>
#include <sstream>
#include <iomanip>
#include "../include/random_walk_filaments.hpp"

using namespace random_walk_filaments;

TextBeadFileWriter::TextBeadFileWriter(std::string output_dir_, unsigned max_file_entries_):
  max_file_entries(max_file_entries_), output_dir(output_dir_) {
  create_output_format_description();
}


TextBeadFileWriter::~TextBeadFileWriter() {
  if (output_file_coords != NULL) output_file_coords->close();
  if (output_file_index_links != NULL) output_file_index_links->close();
}


void TextBeadFileWriter::push_bead_states(const std::vector<Bead> &beads,
					  const std::vector<Filament> &filaments,
					  unsigned t,
					  unsigned n_valid_beads) {
  if (current_file_entries >= max_file_entries)
    close_files();
  if (output_file_index_links == NULL || output_file_coords == NULL) {
    open_next_files();
  }
  // for (const Bead &b: beads) {
  for (std::vector<Bead>::const_iterator it = beads.begin(); it != beads.begin() + n_valid_beads; it++) {
    const Bead &b = *it;
    Vector3D v =  b.position;
    *output_file_coords << v.x << " "
			<< v.y << " "
			<< v.z << " ";
    *output_file_index_links << b.previous << " "
			     << b.next << " "
			     << b.cross_filament << " ";
  }
  *output_file_coords << std::endl;
  *output_file_index_links << std::endl;
  current_file_entries += 3 * beads.size();
}

void TextBeadFileWriter::close_files() {
  output_file_coords->close();
  output_file_coords = NULL;
  output_file_index_links->close();
  output_file_index_links = NULL;
  current_file_entries = 0;
}

void TextBeadFileWriter::open_next_files() {
  std::ostringstream file_name_coords;
  std::ostringstream file_name_index_links;  
  
  file_name_coords << output_dir << "/"
		   << FILE_PREFIX_COORDS
		   << std::setw(6) << std::setfill('0')
		   << current_index
		   << FILE_SUFFIX;
  output_file_coords = new std::ofstream(file_name_coords.str());

  file_name_index_links << output_dir << "/"
		   << FILE_PREFIX_INDEX_LINKS
		   << std::setw(6) << std::setfill('0')
		   << current_index
		   << FILE_SUFFIX;
  output_file_index_links = new std::ofstream(file_name_index_links.str());
  current_index ++;
}

void TextBeadFileWriter::create_output_format_description() {
  std::ofstream description_stream(output_dir + "/" + FORMAT_DESCRIPTION_FILE);
  description_stream << "# random_walk_filaments " << VERSION << std::endl
		     << "# ascii" << std::endl
		     << "# Coordinates" << std::endl
		     << "coordinates of beads in files bead_coords_*.txt."
		     << std::endl
		     << " - one line per time step" << std::endl
		     << " - three floats per coordinate " << std::endl
		     << std::endl
		     << "# Bead links" << std::endl
		     << "Binding of beads is stored in bead_links_*.txt."
		     << std::endl
		     << " - one line per time step" << std::endl
		     << " - three indices (integer) per bead:" << std::endl
		     << "    1. link to previous bead" << std::endl
		     << "    2. link to next bead" << std::endl
		     << "    3. cross-link to bead in other filament" << std::endl
		     << " - positive value points to index of a bead" << std::endl
		     << " - -1 means no link" << std::endl
		     << std::endl;
  description_stream.close();
}
