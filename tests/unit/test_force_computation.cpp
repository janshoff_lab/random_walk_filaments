#include <iostream>
#include <catch.hpp>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;

TEST_CASE( "compute_forward_stretch", "[ForceComputer]" ) {
  System s(0, 0);
  s.set_k_stretch(1.0);

  Vector3D v_n;
  v_n.x = .9;
  v_n.y = .41;
  v_n.z = 0.02;

  /*  F_stretch towards next bead at v_n = {.9, 0.41, 0.02}.
   *  |v_n| = 0.98919
   *  => f_s_n = {-0.91967, -0.41896, -0.02044}
   */
  Vector3D f_s = s.compute_forward_stretch(v_n);
  REQUIRE( Approx(-0.0098339)   == f_s.x );
  REQUIRE( Approx(-0.00447987)  == f_s.y );
  REQUIRE( Approx(-0.00021853)  == f_s.z );
}

TEST_CASE( "compute_forward_bend", "[ForceComputer]" ) {
  System s(0, 0);
  s.set_k_bend(1.0);

  Vector3D v_p, v_n, v_nn;
  v_p.x  = 8.9;
  v_p.y  = -7.2;
  v_p.z  = 0.5;
  v_n.x  = 12.1;
  v_n.y  = 5.4;
  v_n.z  = 4.1;
  v_nn.x = -3.1;
  v_nn.y = 11.8;
  v_nn.z = 3.0;
  
  Vector3D f_b = s.compute_forward_bend(v_p, v_n, v_nn);

  /* See calculation in jupyter notebook in this folder!
   * f_b should be:
   * f_b = [ 0.01172817 -0.02313597 -0.01188616] 
   */

  REQUIRE( Approx(0.0091718) == f_b.x );
  REQUIRE( Approx(-0.01340527) == f_b.y );
  REQUIRE( Approx(-0.00941226) == f_b.z );

  // Case: no previous beads -> no v_p
  //    -> f_b = [ 0.04379826 -0.08501698 -0.01728469] 
  v_p = Vector3D::nan();
  f_b = s.compute_forward_bend(v_p, v_n, v_nn);
  REQUIRE( Approx(0.04379826) == f_b.x );
  REQUIRE( Approx(-0.08501698) == f_b.y );
  REQUIRE( Approx(-0.01728469) == f_b.z );

  // Case: Center of 3-bead filament
  //       -> should lead to f_b =[-0.03462646  0.07161171  0.00787243]
  v_p.x  = 8.9;
  v_p.y  = -7.2;
  v_p.z  = 0.5;
  v_nn = Vector3D::nan();
  f_b = s.compute_forward_bend(v_p, v_n, v_nn);
  REQUIRE( Approx(-0.03462646) == f_b.x );
  REQUIRE( Approx(0.07161171) == f_b.y );
  REQUIRE( Approx(0.00787243) == f_b.z );

  // Case: 2 beads only -> no angles -> no f_b
  v_p = Vector3D::nan();
  f_b = s.compute_forward_bend(v_p, v_n, v_nn);
  REQUIRE( 0.0 == f_b.x );
  REQUIRE( 0.0 == f_b.y );
  REQUIRE( 0.0 == f_b.z );
  v_p.x  = 8.9;
  v_p.y  = -7.2;
  v_p.z  = 0.5;
  v_n = Vector3D::nan();
  f_b = s.compute_forward_bend(v_p, v_n, v_nn);
  REQUIRE( 0.0 == f_b.x );
  REQUIRE( 0.0 == f_b.y );
  REQUIRE( 0.0 == f_b.z );
}
