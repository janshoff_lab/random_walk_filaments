#include <catch.hpp>
#include <vector>
#include <algorithm>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;

TEST_CASE ("shuffled index vector", "[RandomWrapper]") {
  RandomWrapper rw;
  rw.set_seed(0);
  rw.initialize();
  unsigned repeats = 100;
  unsigned size = 100;
  for (unsigned i = 0; i < repeats; i++) {
    std::vector<unsigned> indices = rw.generate_shuffled_index_vector(size);
    for (unsigned j = 0; j < size; j++) {
      REQUIRE( std::find( indices.begin(), indices.end(), j) != indices.end() );
    }
  }
}
