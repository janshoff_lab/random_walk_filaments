#include <iostream>
#include <catch.hpp>
#include <cmath>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;

TEST_CASE( "nan and isnan", "[Vector3D]" ) {  
  Vector3D u;
  u.x = 12.2;
  u.y = 16.2;
  u.z = 2.2222;
  REQUIRE( !u.isnan() );

  u = Vector3D::nan();
  REQUIRE( u.isnan() );
}

TEST_CASE( "minimum image projections", "[Vector3D]" ) {

  Vector3D box; box.x = 1000.; box.y = 1000.; box.z = 1000.0;
  
  Vector3D v1; v1.x = 520.0; v1.y = 80.0; v1.z = 950.0;
  Vector3D v2; v2.x = 700.0; v2.y = 100.0; v2.z = 800.0;

  Vector3D v = Vector3D::minimum_image_vector(v1, v2, box);

  REQUIRE( 180.0 == v.x );
  REQUIRE( 20.0  == v.y );
  REQUIRE( -150.0 == v.z );

  double d = Vector3D::minimum_image_distance_squared(v1, v2, box);

  REQUIRE( 55300.0 == d );

  v2.x = 10.0;

  v = Vector3D::minimum_image_vector(v1, v2, box);
  REQUIRE( 490.0 == v.x );    
  d = Vector3D::minimum_image_distance_squared(v1, v2, box);
  REQUIRE( 263000.0 == d );

  v2.y = 980.0;
  v = Vector3D::minimum_image_vector(v1, v2, box);
  REQUIRE( -100.0 == v.y );
  d = Vector3D::minimum_image_distance_squared(v1, v2, box);
  REQUIRE( 272600.0 == d );

  v2.z = 20.0;
  v = Vector3D::minimum_image_vector(v1, v2, box);
  REQUIRE( 70.0 == v.z );
  d = Vector3D::minimum_image_distance_squared(v1, v2, box);
  REQUIRE( 255000.0 == d );

  v2.x = 5010.0;
  v2.y = -44020.0;
  v2.z = -44980.0;
  v = Vector3D::minimum_image_vector(v1, v2, box);
  REQUIRE( 490.0 == v.x );
  REQUIRE( -100.0 == v.y );
  REQUIRE( 70.0 == v.z );
  d = Vector3D::minimum_image_distance_squared(v1, v2, box);
  REQUIRE( 255000.0 == d );
}

