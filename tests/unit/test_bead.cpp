#include <catch.hpp>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;

TEST_CASE( "copy_properties", "[Bead]" ) {
  Bead b1;
  Bead b2;
  Vector3D v1;
  Vector3D v2;
  v1.x = 2.0;
  v1.y = 3.0;
  v1.z = 4.0;
  v2.x = 5.0;
  v2.y = 6.0;
  v2.z = 7.0;
  
  
  b1.position = v1;
  b2.position = v2;

  b1.state = BeadState::Free;
  b2.state = BeadState::FilamentBegin;

  b1.previous = -1;
  b1.next = -1;
  b1.cross_filament = -1;
  b1.id = 25;

  b2.previous = -1;
  b2.next = 5;
  b2.cross_filament = 12;
  b2.id = 94;

  b1.copy_properties(b2);

  REQUIRE( b1.position.x == 5.0);
  REQUIRE( b1.position.y == 6.0);
  REQUIRE( b1.position.z == 7.0);
  REQUIRE( b1.previous == -1);
  REQUIRE( b1.next == 5);
  REQUIRE( b1.cross_filament == 12);
  REQUIRE( b1.state == BeadState::FilamentBegin);
  REQUIRE( b1.id == 94 );
}


