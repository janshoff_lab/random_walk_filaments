
#include <catch.hpp>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;

TEST_CASE( "check_for_beads_on_path", "[CollisionHandler]" ) {

  std::vector<Bead> beads;
  Vector3D box; box.x = 1000.0; box.y = 1000.0; box.z = 1000.0;

  Vector3D v0; v0.x = 497.8; v0.y = 500.0; v0.z = 500.0;
  Vector3D v1; v1.x = 498.9; v1.y = 500.0; v1.z = 500.0;
  Vector3D v2; v2.x = 500.0; v2.y = 500.0; v2.z = 500.0;
  Vector3D v3; v3.x = 501.1; v3.y = 500.0; v3.z = 500.0;
  Vector3D v4; v4.x = 502.2; v4.y = 500.0; v4.z = 500.0;

  Bead b0, b1, b2, b3, b4;
  b0.position = v0;
  b1.position = v1;
  b2.position = v2;
  b3.position = v3;
  b4.position = v4;

  beads.push_back(b0);
  beads.push_back(b1);
  beads.push_back(b2);
  beads.push_back(b3);
  beads.push_back(b4);  
  
  CollisionHandler ch;
  ch.beads = &beads;
  ch.box = &box;
  Logger logger;
  logger.set_log_stream(&std::cout);
  logger.set_log_up_to(Logger::LOG_LEVEL_INFO);
  ch.logger = &logger;

  Vector3D displ; displ.x = 2.0; displ.y = 0.0; displ.z = 0.0;

  std::vector<unsigned> other_indices;
  other_indices.push_back(0); other_indices.push_back(1);
  other_indices.push_back(3); other_indices.push_back(4);
						  
  std::multimap<double, unsigned> bds_in_path = ch.check_for_beads_in_path(2, v2,
									   displ, other_indices);

  REQUIRE( 2 == bds_in_path.size() );

  std::multimap<double, unsigned>::const_iterator it = bds_in_path.begin();

  REQUIRE( 3 == it->second );
  REQUIRE( Approx(0.05) == it->first );

  std::advance(it, 1);
  REQUIRE( 4 == it->second );
  REQUIRE( Approx(0.6) == it->first );

  displ.z = 2.0;

  bds_in_path = ch.check_for_beads_in_path(2, v2, displ, other_indices);
  REQUIRE( 1 == bds_in_path.size() );
  REQUIRE( 3 == bds_in_path.begin()->second );
  REQUIRE( Approx(0.052795139) == bds_in_path.begin()->first );
  
  
}
