#include <iostream>
#include <catch.hpp>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;

TEST_CASE( "bead_line_to_bead_object", "[System]" ) {
  std::string line = "42\t-13.5\t9.2\t17.002\t-1\t5\t-1";
  std::istringstream line_stream(line);
  Bead b = System::bead_line_to_bead_object(line_stream);
  REQUIRE( 42 == b.id );
  REQUIRE( Approx(-13.5) == b.position.x );
  REQUIRE( Approx(9.2)   == b.position.y );
  REQUIRE( Approx(17.002)== b.position.z );
  REQUIRE( -1 == b.previous );
  REQUIRE( 5  == b.next );
  REQUIRE( -1 == b.cross_filament );
}

TEST_CASE( "filament_line_to_filament_object", "[System]" ) {
  std::string line = "12\t144\t169\t55";
  std::istringstream line_stream(line);
  Filament f = System::filament_line_to_filament_object(line_stream);
  REQUIRE( 12 == f.id );
  REQUIRE( 144 == f.first );
  REQUIRE( 169 == f.last );
  REQUIRE( 55  == f.n_beads_total );
}
