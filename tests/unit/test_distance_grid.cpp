#include <stdexcept>
#include <algorithm>
#include <catch.hpp>
#include <iostream>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;

std::vector<Bead> coordinates_vector_to_beads(const std::vector<double> &coordinates) {
  if ((coordinates.size() % 3) != 0) {
    throw std::runtime_error("coordinates vector's size has to be multiple of 3");
  }
  std::vector<Bead> beads;
  for ( unsigned i = 0; i < coordinates.size(); i+=3 ) {
    Bead b;
    Vector3D v;
    v.x = coordinates[i];
    v.y = coordinates[i + 1];
    v.z = coordinates[i + 2];
    b.position = v;
    beads.push_back(b);
  }
  return beads;
}

std::vector<Bead> generate_test_beads() {  

  std::vector<double> coordinates = { 30.83256848,  148.0099393 ,   52.66300304,
				      106.74787868,    2.9149925 ,  183.74940162,			      
				      180.14297082,    6.68428553,  191.38986726,
				      27.44186427,   56.76567059,  121.21663687,
				      188.84502721,  170.54710822,    0.4518467 ,
				      104.24520544,  110.40752665,   97.07548273,
				      153.62683081,   32.14335063,  152.91209007,
				      4.16195959,   27.04203567,   23.25460348,
				      61.9795169 ,  134.29052904,   94.24595565,
				      163.23365961,   57.91735671,  146.62519552,
				      140.52447105,   65.51389525,   66.92950582,
				      195.6116158 ,  124.91642235,  190.06270494,
				      153.49513012,  165.00185064,   81.32806036,
				      90.26168228,   80.12632547,  199.02763207,
				      35.51283519,  192.51938061,   83.85005405,
				      84.81048931,   92.62977395,   74.74462976,
				      93.10162003,    7.03365229,   16.85453395,
				      146.50413963,  127.23999877,    5.58155777,
				      60.03401196,   44.17050417,   11.00399867,
				      104.64921416,   83.27393146,    9.64374997};    
  return coordinates_vector_to_beads(coordinates);
}

TEST_CASE ( "initialize DistanceGrid", "[DistanceGrid]") {
  Vector3D box; box.x = 1000.; box.y = 2000.; box.z = 1000.;
  cell_index_t n_cells = {10, 20, 10};
  DistanceGrid dg(n_cells, box);  
}

TEST_CASE( "coords to cell index", "[DistanceGrid]") {
  Vector3D box; box.x = 1000.; box.y = 2000.; box.z = 1000.;
  cell_index_t n_cells = {10, 20, 10};
  DistanceGrid dg(n_cells, box);
  Vector3D v; v.x = 250.; v.y = 350.; v.z = 450.;
  cell_index_t cidx = dg.coords_to_cidx(v);
  REQUIRE( 2 == cidx[0] );
  REQUIRE( 3 == cidx[1] );
  REQUIRE( 4 == cidx[2] );

  v.x = 650.; v.y = 1650.; v.z = 920.;
  cidx = dg.coords_to_cidx(v);
  REQUIRE( 6 == cidx[0] );
  REQUIRE( 16 == cidx[1] );
  REQUIRE( 9 == cidx[2] );

  v.x = -650.; v.y = 1650.; v.z = 920.;
  cidx = dg.coords_to_cidx(v);
  REQUIRE( 3 == cidx[0] );
  REQUIRE( 16 == cidx[1] );
  REQUIRE( 9 == cidx[2] );

  v.x = -250.; v.y = 1650.; v.z = 920.;
  cidx = dg.coords_to_cidx(v);
  REQUIRE( 7 == cidx[0] );
  REQUIRE( 16 == cidx[1] );
  REQUIRE( 9 == cidx[2] );

}

TEST_CASE( "get forward neighbors", "[DistanceGrid]" ) {
  Vector3D box; box.x = 1000.; box.y = 2000.; box.z = 1000.;
  cell_index_t n_cells = {10, 10, 5};
  DistanceGrid dg(n_cells, box);

  /*
   *==================================================================================
   *                         CASE: cell_index in corner
   *==================================================================================
   */

  cell_index_t cell_index = {0, 0, 0};
  std::vector<cell_index_t> neighbors = dg.get_forward_neighbors(cell_index);
  REQUIRE( 13 == neighbors.size() );

  cell_index_t compare_index;
  // first plane: y = 0
  compare_index[1] = 0;
  {
    // first column: x = 0;
    compare_index[0] = 0;
    {
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // second column: x = -1
    compare_index[0] = 9; // == (x - 1) % n_cells_x due to periodic boundaries
    {
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // third column
    compare_index[0] = 1;
    {
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
  }

  // second plane: y = -1
  compare_index[1] = 9; // == (y - 1) % n_cells_y due to periodic boundaries
  {
    // first column: x = 0;
    compare_index[0] = 0;
    {    
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // second column: x = -1
    compare_index[0] = 9; // == (x - 1) % n_cells_x due to periodic boundaries
    {
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // third column
    compare_index[0] = 1;
    {
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
  }

  // last plane: y = 1
  compare_index[1] = 1;
  {
    // first column: x = 0;
    compare_index[0] = 0;
    {    
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // second column: x = -1
    compare_index[0] = 9; // == (x - 1) % n_cells_x due to periodic boundaries
    {
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // third column
    compare_index[0] = 1;
    {
      compare_index[2] = 0;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 4; // == (z - 1) % n_cells_z due to periodic boundaries
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
  }

  /*
    =======================================================================================
                                 CASE: cell_index not at a border
    =======================================================================================
  */
  cell_index[0] = 6;
  cell_index[1] = 3;
  cell_index[2] = 2;

  neighbors = dg.get_forward_neighbors(cell_index);
  REQUIRE( 13 == neighbors.size() );

  // first plane: yi = y;
  compare_index[1] = 3;
  {
    // first column: xi = x;
    compare_index[0] = 6;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // second column: xi = x - 1
    compare_index[0] = 5; 
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // third column: xi = x + 1
    compare_index[0] = 7;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
  }

  // second plane: yi = y - 1
  compare_index[1] = 2; 
  {
    // first column: xi = x;
    compare_index[0] = 6;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // second column: xi = x - 1
    compare_index[0] = 5; 
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // third column: xi = x + 1;
    compare_index[0] = 7;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
  }

  // last plane: yi = y + 1
  compare_index[1] = 4;
  {
    // first column: xi = x;
    compare_index[0] = 6;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // second column: xi = x - 1
    compare_index[0] = 5; 
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // third column: xi = x + 1
    compare_index[0] = 7;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
  }

  /*
    ========================================================================================
                               CASE: cell_index one dimension at border
    ========================================================================================
  */
  
  cell_index[0] = 9;
  cell_index[1] = 3;
  cell_index[2] = 2;
  
  neighbors = dg.get_forward_neighbors(cell_index);
  REQUIRE( 13 == neighbors.size() );

  // first plane: yi = y;
  compare_index[1] = 3;
  {
    // first column: xi = x;
    compare_index[0] = 9;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // second column: xi = x - 1
    compare_index[0] = 8; 
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // third column: xi = x + 1
    compare_index[0] = 0; // == (x + 1) % n_cells_x due to periodic boundaries
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
  }

  // second plane: yi = y - 1
  compare_index[1] = 2; 
  {
    // first column: xi = x;
    compare_index[0] = 9;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // second column: xi = x - 1
    compare_index[0] = 8; 
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
    // third column: xi = x + 1;
    compare_index[0] = 0; // == (x + 1) % n_cells_x due to periodic boundaries
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 1;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) == neighbors.end() );
    }
  }

  // last plane: yi = y + 1
  compare_index[1] = 4;
  {
    // first column: xi = x;
    compare_index[0] = 9;
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // second column: xi = x - 1
    compare_index[0] = 8; 
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
    // third column: xi = x + 1
    compare_index[0] = 0; // == (x + 1) % n_cells_x due to periodic boundaries
    {
      compare_index[2] = 2;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 1; 
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
      compare_index[2] = 3;
      REQUIRE( std::find(neighbors.begin(), neighbors.end(), compare_index) != neighbors.end() );
    }
  }
  
}

TEST_CASE( "add and retrieve bead indices", "[DistanceGrid]" ) {
  Vector3D box; box.x = 1000.; box.y = 2000.; box.z = 1000.;
  cell_index_t n_cells = {10, 20, 10};
  DistanceGrid dg(n_cells, box);

  std::vector<Bead> beads;

  Vector3D v0; v0.x = 714.; v0.y = 1229.; v0.z = 152.;  
  Vector3D v1; v1.x = 699.; v1.y = 1230.; v1.z = 160.;
  Vector3D v2; v2.x = 780.; v2.y = 914.;  v2.z = 300.;

  Bead b0; b0.position=v0; beads.push_back(b0);
  Bead b1; b1.position=v1; beads.push_back(b1);
  Bead b2; b2.position=v2; beads.push_back(b2);
  
  std::cout << "adding bead ids" << std::endl;
  
  dg.update_cell_map(beads, 3);
  
  cell_index_t cidx = {7, 12, 1};
  std::cout << "fetching bead ids" << std::endl;
  std::vector<unsigned> ids = dg.get_bead_indices(cidx);
 
  REQUIRE( 1 == ids.size() );
  REQUIRE( 0 == ids.at(0)  );

  cidx = (cell_index_t) {9, 19, 9};

  ids = dg.get_bead_indices(cidx);
  REQUIRE( 0 == ids.size() );

  Vector3D v3; v3.x =  14.; v3.y =  29.; v3.z =  52.; 
  Vector3D v4; v4.x = 1024.; v4.y = 2029.; v4.z = 1052.;
  Vector3D v5; v5.x = -14.; v5.y = -29.; v5.z = -52.;

  Bead b3; b3.position=v3; beads.push_back(b3);
  Bead b4; b4.position=v4; beads.push_back(b4);
  Bead b5; b5.position=v5; beads.push_back(b5);

  dg.update_cell_map(beads, 6);

  ids = dg.get_bead_indices(cidx);
  REQUIRE( 1 == ids.size() );
  REQUIRE( 5 == ids.at(0)  );
}

TEST_CASE( "cell indices complete" , "[DistanceGrid]") {
  Vector3D box; box.x = 1000.; box.y = 2000.; box.z = 1000.;
  cell_index_t n_cells = {10, 20, 10};
  DistanceGrid dg(n_cells, box);
  
  std::vector<cell_index_t> cidxs = dg.get_cell_indices();
  REQUIRE( 2000 == cidxs.size() );
  bool none_missing = true;
  for (unsigned i = 0; i < n_cells[0]; i++) {
    for (unsigned j = 0; j < n_cells[1]; j++) {
      for (unsigned k = 0; k < n_cells[2]; k++) {
	cell_index_t idx = {i, j, k};
	std::vector<cell_index_t>::const_iterator it = std::find( cidxs.begin(), cidxs.end(), idx );
	if (it == cidxs.end()) none_missing = false;
      }
    }
  }
  REQUIRE( none_missing );
}

/*
TEST_CASE( "Visit all pairs in range", "[DistanceGrid]" ) {
  Vector3D box; box.x = 50.; box.y = 50.; box.z = 50.;
  cell_index_t n_cells = {5, 5, 5};
  DistanceGrid dg(n_cells, box);

  double cutoff_squared = 225.0;

  std::vector<Bead> beads = generate_test_beads();
  // w/o distance grid, find all pairs closer than cutoff
  std::multimap<unsigned, unsigned> pairs_in_range;
  for (unsigned i=0; i<beads.size()-1; i++) {
    for (unsigned j=i+1; j<beads.size(); j++) {
      const Bead& bi = beads.at(i);
      const Bead& bj = beads.at(j);
      Vector3D v = Vector3D::minimum_image_vector(bi.position, bj.position, box);
      double r = v.norm_squared();
      if (v.norm_squared() > cutoff_squared) continue;
      pairs_in_range.insert(std::pair<unsigned, unsigned>(i, j));
    }
  }

  const std::vector<cell_index_t>& cell_indices =  dg.get_cell_indices();
  for (const cell_index_t& cidx: cell_indices) {
    for (const cell_index_t& cidx_
  }
  
  REQUIRE( false );

}
*/
