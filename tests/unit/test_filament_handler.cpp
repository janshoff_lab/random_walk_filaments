#include <iostream>
#include <catch.hpp>
#include <random_walk_filaments.hpp>

using namespace random_walk_filaments;


TEST_CASE( "reserve_space_for_beads", "[FilamentHandler]" ) {  
  FilamentHandler fh;
  fh.reserve_space_for_beads_and_filaments(15, 10);
  REQUIRE(15 == fh.beads.size());
  for (const Bead &b: fh.beads) {
    REQUIRE(b.position.isnan());
    REQUIRE(-1 == b.next);
    REQUIRE(-1 == b.previous);
    REQUIRE(BeadState::Free == b.state);
  }
  REQUIRE(0 == fh.n_valid_beads);
}

TEST_CASE( "get_new_bead_location", "[FilamentHandler]" ) {
  RandomWrapper rw;
  rw.set_seed(112);
  rw.initialize();

  double segment_length_std = 0.1;

  Vector3D v_p;
  Vector3D v_pp;
  Vector3D box_size;

  box_size.x = 100.0;
  box_size.y = 200.0;
  box_size.z = 300.0;

  FilamentHandler fh;
  fh.box_size = box_size;
  fh.random_wrapper = &rw;
  fh.new_segment_length_std = segment_length_std;

  v_pp.x = .5;
  v_pp.y = 1.0;
  v_pp.z = 1.5;

  v_p.x = 1.5;
  v_p.y = 1.5;
  v_p.z = 1.5;

  // all following calculations need to be divided by 20!
  // diff = (20.0, 10.0, 0.0)
  // normalized: (0.89442719, 0.4472136, 0.0)
  // first length drawn: l = 17.9592
  // => v = v_p + l * (0.89442719, 0.4472136, 0.0)
  //      = (46.06319681, 38.0315984 , 30.)
  // PLUS SOME NOISE! NOT CHECKED FOR THAT YET
  // I JUST ADJUSTED THE VALUES SLIGHTLY ACCORDINGLY

  Vector3D v = fh.get_new_bead_location(v_p, v_pp);
  REQUIRE( Approx(2.2992244) == v.x );
  REQUIRE( Approx(1.94796)   == v.y );
  REQUIRE( Approx(1.51825)   == v.z );

  // second length drawn: l = 19.8406

  v_p.x = 0.25;
  v_p.y = 1.0;
  v_p.z = 0.25;

  // divide by 20!
  // diff = (5.0, 0.0, 25.0)
  // normalized: (0.19611614, 0., 0.98058068)
  // second length drawn: l = 19.8406
  // => v = v_p + l * (0.19611614, 0., 0.98058068)
  //      = (1.10893821,  20., -14.45530895)
  // PLUS SOME NOISE! NOT CHECKED FOR THAT YET
  // I JUST ADJUSTED THE VALUES SLIGHTLY ACCORDINGLY

  v = fh.get_new_bead_location(v_p, v_pp);
  REQUIRE( Approx(0.02027)    == v.x );
  REQUIRE( Approx(0.983593)   == v.y );
  REQUIRE( Approx(-0.7963348) == v.z );  
}

TEST_CASE( "add_bead_to_filament", "[FilamentHandler]" ) {
  RandomWrapper rw;
  rw.set_seed(112);
  rw.initialize();

  double segment_length_std = 0.1;

  Vector3D v_p;
  Vector3D v_pp;
  Vector3D box_size;

  box_size.x = 5.0;
  box_size.y = 10.0;
  box_size.z = 15.0;

  FilamentHandler fh;
  Logger logger;
  fh.logger = &logger;
  fh.box_size = box_size;
  fh.random_wrapper = &rw;
  fh.new_segment_length_std = segment_length_std;
  fh.reserve_space_for_beads_and_filaments(15, 10);

  Bead &b0 = fh.beads.at(0);
  Vector3D v0; v0.x = 1.2; v0.y = 119.0/20., v0.z = 130.0/20.;
  b0.position = v0;

  Bead &b1 = fh.beads.at(1);
  Vector3D v1; v1.x = 1.5; v1.y = 1.5; v1.z = 1.5;
  b1.position = v1;
  b1.previous = 2;
  b1.state = BeadState::FilamentEnd;

  Bead &b2 = fh.beads.at(2);
  Vector3D v2; v2.x = 0.5; v2.y = 1.0; v2.z = 1.5;
  b2.position = v2;
  b2.next = 1;
  b2.state = BeadState::FilamentBegin;

  fh.n_valid_beads = 3;  

  Filament f;
  f.first = 2;
  f.last = 1;
  f.id = 14;
  f.n_beads_total = 2;

  fh.filaments.push_back(f);  

  REQUIRE( BeadState::FilamentEnd == fh.beads.at(1).state );
  fh.add_bead_to_filament(14);


  REQUIRE(4 == fh.n_valid_beads);
  
  REQUIRE(14 == fh.filaments.at(0).id);
  REQUIRE(2 == fh.filaments.at(0).first);
  REQUIRE(3 == fh.filaments.at(0).last);
  REQUIRE(3 == fh.filaments.at(0).n_beads_total);

  REQUIRE(3 == fh.beads.at(1).next);
  REQUIRE(1 == fh.beads.at(3).previous);
  Vector3D v = fh.beads.at(3).position;
  // NOT CALCULATED EXACTLY SINCE NOISE IS ADDED
  REQUIRE( Approx(2.4827)   == v.x );
  REQUIRE( Approx(1.978430) == v.y );
  REQUIRE( Approx(1.555035) == v.z );
  REQUIRE( BeadState::FilamentEnd == fh.beads.at(3).state );
  REQUIRE( BeadState::FilamentInterior == fh.beads.at(1).state );

  // shift beads around, to match the test above ("get_new_bead_location")
  fh.beads.at(1).position = fh.beads.at(2).position;
  fh.beads.at(2).position.x = 0.25;
  fh.beads.at(2).position.y = 1.0;
  fh.beads.at(2).position.z = 0.25;

  REQUIRE( BeadState::FilamentBegin == fh.beads.at(2).state );
  fh.add_bead_to_filament(14, BeadState::FilamentBegin);
  
  REQUIRE(5 == fh.n_valid_beads);
  REQUIRE(14 == fh.filaments.at(0).id);
  REQUIRE(4 == fh.filaments.at(0).first);
  REQUIRE(3 == fh.filaments.at(0).last);
  REQUIRE(4 == fh.filaments.at(0).n_beads_total);  

  REQUIRE(2 == fh.beads.at(4).next);
  REQUIRE(4 == fh.beads.at(2).previous);
  REQUIRE( BeadState::FilamentBegin == fh.beads.at(4).state );
  REQUIRE( BeadState::FilamentInterior == fh.beads.at(2).state );
  v = fh.beads.at(4).position;

  REQUIRE( Approx(0.0164552) == v.x );
  REQUIRE( Approx(1.00201)   == v.y );
  REQUIRE( Approx(-0.863491) == v.z );  
    
}


TEST_CASE( "remove_bead_from_filament", "[FilamentHandler]") {
  Logger logger;
  logger.set_log_stream(&std::cout);
  logger.set_log_up_to(Logger::LOG_LEVEL_INFO);
  
  RandomWrapper rw;
  rw.set_seed(112);
  rw.initialize();

  double segment_length_std = 0.1;

  Vector3D v_p;
  Vector3D v_pp;
  Vector3D box_size;

  box_size.x = 5.0;
  box_size.y = 10.0;
  box_size.z = 15.0;

  FilamentHandler fh;
  fh.box_size = box_size;
  fh.random_wrapper = &rw;
  fh.logger = &logger;
  fh.new_segment_length_std = segment_length_std;
  fh.reserve_space_for_beads_and_filaments(15, 10);

  Bead &b0 = fh.beads.at(0);
  b0.state = BeadState::FilamentEnd;
  Vector3D v0; v0.x = 24./20.; v0.y = 119.0/20., v0.z = 130.0/20.;
  b0.position = v0;
  b0.next = -1;
  b0.previous = 5;

  Bead &b1 = fh.beads.at(1);
  b1.state = BeadState::FilamentInterior;
  Vector3D v1; v1.x = 1.5; v1.y = 1.5; v1.z = 1.5;
  b1.position = v1;
  b1.previous = 3;
  b1.next = 2;

  Bead &b2 = fh.beads.at(2);
  b2.state = BeadState::FilamentInterior;
  Vector3D v2; v2.x = 0.5; v2.y = 1.0; v2.z = 1.5;
  b2.position = v2;
  b2.next = 4;
  b2.previous = 1;

  Bead &b3 = fh.beads.at(3);
  b3.state = BeadState::FilamentBegin;
  Vector3D v3; v3.x = 15.0/20.; v3.y = 16.0/20.; v3.z = 17.0/20.;
  b3.position = v3;
  b3.previous = -1;
  b3.next = 1;

  Bead &b4 = fh.beads.at(4);
  b4.state = BeadState::FilamentEnd;
  Vector3D v4; v4.x = 15.0/20.; v4.y = 16.0/20.; v4.z = 17.0/20.;
  b4.position = v4;
  b4.previous = 2;

  Bead &b5 = fh.beads.at(5);
  b5.state = BeadState::FilamentBegin;
  Vector3D v5; v5.x = 15.0/20.; v5.y = 16.0/20.; v5.z = 17.0/20.;
  b5.position = v5;
  b5.previous = -1;
  b5.next = 0;
    
  fh.n_valid_beads = 6;  

  Filament f0;
  f0.first = 3;
  f0.last = 4;
  f0.id = 14;
  f0.n_beads_total = 4;

  fh.filaments.push_back(f0);

  Filament f1;
  f1.first = 5;
  f1.last = 0;
  f1.id = 19;
  f1.n_beads_total = 2;

  fh.filaments.push_back(f1);

  REQUIRE( 2 == fh.filaments.size() );
  
  fh.remove_bead_from_filament(14, BeadState::FilamentEnd);

  REQUIRE( 5 == fh.n_valid_beads );
  REQUIRE( 3 == fh.filaments.at(0).n_beads_total );
  REQUIRE( 2 == fh.filaments.at(0).last );

  fh.remove_bead_from_filament(14, BeadState::FilamentBegin);

  REQUIRE( 4 == fh.n_valid_beads );
  REQUIRE( 2 == fh.filaments.at(0).n_beads_total );
  REQUIRE( 2 == fh.filaments.at(0).last );
  REQUIRE( 1 == fh.filaments.at(0).first );

  fh.remove_bead_from_filament(14, BeadState::FilamentEnd);

  REQUIRE( 2 == fh.n_valid_beads );
  REQUIRE( 1 == fh.filaments.size() );

  fh.remove_bead_from_filament(19, BeadState::FilamentEnd);

  REQUIRE( 0 == fh.n_valid_beads );
  REQUIRE( 0 == fh.filaments.size() );
}

TEST_CASE( "remove_bead_from_filament - single filament", "[FilamentHandler]" ) {
  Logger logger;
  logger.set_log_stream(&std::cout);
  logger.set_log_up_to(Logger::LOG_LEVEL_INFO);
  
  RandomWrapper rw;
  rw.set_seed(112);
  rw.initialize();

  double segment_length_std = .1;

  Vector3D box_size;

  box_size.x = 5.0;
  box_size.y = 10.0;
  box_size.z = 15.0;

  FilamentHandler fh;
  fh.box_size = box_size;
  fh.random_wrapper = &rw;
  fh.logger = &logger;
  fh.new_segment_length_std = segment_length_std;
  fh.reserve_space_for_beads_and_filaments(15, 10);

  Bead &b0 = fh.beads.at(0);
  b0.state = BeadState::FilamentBegin;
  b0.next = 2;
  b0.previous = -1;
  b0.id = 4;

  Bead &b1 = fh.beads.at(1);
  b1.state = BeadState::FilamentEnd;
  b1.previous = 2;
  b1.next = -1;
  b1.id = 10;

  Bead &b2 = fh.beads.at(2);
  b2.state = BeadState::FilamentInterior;
  b2.next = 1;
  b2.previous = 0;
  b2.id = 9;

  fh.n_valid_beads = 3;

  Filament f;
  f.first = 0;
  f.last = 1;
  f.n_beads_total = 3;
  f.id = 1;

  fh.filaments.push_back(f);

  fh.remove_bead_from_filament(1, BeadState::FilamentBegin);

  REQUIRE( 2 == fh.n_valid_beads );
  REQUIRE( 1 == fh.filaments.size() );
  REQUIRE( 2 == fh.filaments.at(0).n_beads_total );
  REQUIRE( 0 == fh.filaments.at(0).first );
  REQUIRE( 1 == fh.filaments.at(0).last );

  REQUIRE( 9 == fh.beads.at(0).id );
  REQUIRE( -1 == fh.beads.at(0).previous );
  REQUIRE( 1 == fh.beads.at(0).next );

  REQUIRE( 10 == fh.beads.at(1).id );
  REQUIRE( 0 == fh.beads.at(1).previous );
  REQUIRE( -1 == fh.beads.at(1).next );  
}

TEST_CASE( "remove_bead_from_filament - moving around and deleting filament",
	   "[FilamentHandler]" ) {
  /** Case: Delte a bead, which leads to deletion of a 
        filament, where the second bead to be deleted
        is also the last valid bead in the vector
        of beads.
   */
  Logger logger;
  logger.set_log_stream(&std::cout);
  logger.set_log_up_to(Logger::LOG_LEVEL_INFO);
  logger.log(Logger::LOG_LEVEL_DEBUG, "TEST: remove_bead_from_filament - moving around and deleting filament");
  
  RandomWrapper rw;
  rw.set_seed(112);
  rw.initialize();

  double segment_length_std = 0.1;

  Vector3D box_size;

  box_size.x = 5.0;
  box_size.y = 10.0;
  box_size.z = 15.0;

  FilamentHandler fh;
  fh.box_size = box_size;
  fh.random_wrapper = &rw;
  fh.logger = &logger;
  fh.new_segment_length_std = segment_length_std;
  fh.reserve_space_for_beads_and_filaments(15, 10);

  Bead &b0 = fh.beads.at(0);
  b0.state = BeadState::FilamentBegin;
  b0.next = 3;
  b0.previous = -1;
  b0.id = 4;

  Bead &b1 = fh.beads.at(1);
  b1.state = BeadState::FilamentBegin;
  b1.previous = -1;
  b1.next = 2;
  b1.id = 10;

  Bead &b2 = fh.beads.at(2);
  b2.state = BeadState::FilamentEnd;
  b2.next = -1;
  b2.previous = 1;
  b2.id = 9;

  Bead &b3 = fh.beads.at(3);
  b3.state = BeadState::FilamentEnd;
  b3.next = -1;
  b3.previous = 0;
  b3.id = 144;

  fh.n_valid_beads = 4;

  Filament f;
  f.first = 0;
  f.last = 3;
  f.n_beads_total = 2;
  f.id = 98;

  Filament f2;
  f2.first = 1;
  f2.last = 2;
  f2.n_beads_total = 2;
  f2.id = 52;

  fh.filaments.push_back(f);
  fh.filaments.push_back(f2);

  fh.remove_bead_from_filament(98, BeadState::FilamentBegin);

  REQUIRE( 2 == fh.n_valid_beads );
  REQUIRE( 1 == fh.filaments.size() );
  REQUIRE( 2 == fh.filaments.at(0).n_beads_total );
  REQUIRE( 1 == fh.filaments.at(0).first );
  REQUIRE( 0 == fh.filaments.at(0).last );
  REQUIRE( 52 == fh.filaments.at(0).id );

  REQUIRE( 9 == fh.beads.at(0).id );
  REQUIRE( 1 == fh.beads.at(0).previous );
  REQUIRE( -1 == fh.beads.at(0).next );
  REQUIRE( BeadState::FilamentEnd == fh.beads.at(0).state );

  REQUIRE( 10 == fh.beads.at(1).id );
  REQUIRE( -1 == fh.beads.at(1).previous );
  REQUIRE( 0 == fh.beads.at(1).next );
  REQUIRE( BeadState::FilamentBegin == fh.beads.at(1).state );
  
}

TEST_CASE( "create_new_filament", "[FilamentHandler]") {
  RandomWrapper rw;
  rw.set_seed(112);
  rw.initialize();

  double segment_length_std = 0.1;

  Vector3D box_size;

  box_size.x = 5.0;
  box_size.y = 10.0;
  box_size.z = 15.0;

  FilamentHandler fh;
  fh.box_size = box_size;
  fh.random_wrapper = &rw;
  fh.new_segment_length_std = segment_length_std;
  fh.reserve_space_for_beads_and_filaments(15, 10);

  REQUIRE( fh.n_valid_beads == 0 );
  REQUIRE( fh.filaments.size() == 0 );

  fh.create_new_filament();

  REQUIRE( fh.n_valid_beads == 2 );
  REQUIRE( fh.filaments.size() == 1);

  Filament &f0 = fh.filaments.at(0);
  REQUIRE( f0.first == 0 );
  REQUIRE( f0.last  == 1 );
  REQUIRE( f0.id    == 0 );
  REQUIRE( f0.n_beads_total == 2);

  Bead &b0 = fh.beads.at(0);
  Bead &b1 = fh.beads.at(1);

  REQUIRE( b0.next == 1 );
  REQUIRE( b0.previous == -1 );
  REQUIRE( b1.next == -1 );
  REQUIRE( b1.previous == 0 );  

  Vector3D v0 = b0.position;
  REQUIRE( Approx(37.505712/20.)     == v0.x );
  REQUIRE( Approx(141.515107998/20.) == v0.y );
  REQUIRE( Approx(192.091385/20.)    == v0.z );
  Vector3D v1 = b1.position;
  // THE FOLLOWING 3 VALUES DO NOT MATCH THE EXPECTED
  // VALUES EXACTLY, DUE TO ADDED NOISE.
  // I JUST ADJUSTED THE VALUES SLIGHTLY ACCORDINGLY
  REQUIRE( Approx(1.61091)   == v1.x );
  REQUIRE( Approx(6.286473)  == v1.y );
  REQUIRE( Approx(10.224887) == v1.z );

  fh.create_new_filament();

  REQUIRE( fh.n_valid_beads == 4 );
  REQUIRE( fh.filaments.size() == 2);

  Filament &f1 = fh.filaments.at(1);
  REQUIRE( f1.first == 2 );
  REQUIRE( f1.last  == 3 );
  REQUIRE( f1.id    == 1 );
  REQUIRE( f1.n_beads_total == 2);

  Bead &b2 = fh.beads.at(2);
  Bead &b3 = fh.beads.at(3);

  REQUIRE( b2.next == 3 );
  REQUIRE( b2.previous == -1 );
  REQUIRE( b3.next == -1 );
  REQUIRE( b3.previous == 2 );  
}

